EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:al8807_sot
LIBS:LMR14203
LIBS:proj-cache
EELAYER 25 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X04 J1
U 1 1 590C49FE
P 4300 7300
F 0 "J1" H 4300 7550 50  0000 C CNN
F 1 "CONN_01X04" V 4400 7300 50  0000 C CNN
F 2 "" H 4300 7300 50  0001 C CNN
F 3 "" H 4300 7300 50  0001 C CNN
	1    4300 7300
	-1   0    0    1   
$EndComp
Text Label 4550 7150 0    60   ~ 0
+18V
Text Label 4550 7450 0    60   ~ 0
GND
Text Label 4550 7250 0    60   ~ 0
ADJ
Text Label 4550 7350 0    60   ~ 0
CTRL
$Comp
L GND #PWR3
U 1 1 590C4ACF
P 4850 7550
F 0 "#PWR3" H 4850 7300 50  0001 C CNN
F 1 "GND" H 4850 7400 50  0000 C CNN
F 2 "" H 4850 7550 50  0001 C CNN
F 3 "" H 4850 7550 50  0001 C CNN
	1    4850 7550
	1    0    0    -1  
$EndComp
$Comp
L VAA #PWR1
U 1 1 590C4AFA
P 4850 7050
F 0 "#PWR1" H 4850 6900 50  0001 C CNN
F 1 "VAA" H 4850 7200 50  0000 C CNN
F 2 "" H 4850 7050 50  0001 C CNN
F 3 "" H 4850 7050 50  0001 C CNN
	1    4850 7050
	1    0    0    -1  
$EndComp
Text GLabel 5200 7650 3    60   Input ~ 0
ADJ
$Comp
L R R24
U 1 1 590C809B
P 1800 1550
F 0 "R24" V 1880 1550 50  0000 C CNN
F 1 "0" V 1800 1550 50  0000 C CNN
F 2 "" V 1730 1550 50  0001 C CNN
F 3 "" H 1800 1550 50  0001 C CNN
	1    1800 1550
	0    -1   -1   0   
$EndComp
$Comp
L R R23
U 1 1 590C80B3
P 2150 1550
F 0 "R23" V 2050 1550 50  0000 C CNN
F 1 "0" V 2150 1550 50  0000 C CNN
F 2 "" V 2080 1550 50  0001 C CNN
F 3 "" H 2150 1550 50  0001 C CNN
	1    2150 1550
	0    1    1    0   
$EndComp
Text GLabel 2350 1550 2    60   Input ~ 0
ADJ
$Comp
L R R20
U 1 1 590C80BC
P 3150 1550
F 0 "R20" V 3230 1550 50  0000 C CNN
F 1 "0" V 3150 1550 50  0000 C CNN
F 2 "" V 3080 1550 50  0001 C CNN
F 3 "" H 3150 1550 50  0001 C CNN
	1    3150 1550
	-1   0    0    1   
$EndComp
$Comp
L VAA #PWR14
U 1 1 590C80CC
P 3150 1350
F 0 "#PWR14" H 3150 1200 50  0001 C CNN
F 1 "VAA" H 3150 1500 50  0000 C CNN
F 2 "" H 3150 1350 50  0001 C CNN
F 3 "" H 3150 1350 50  0001 C CNN
	1    3150 1350
	1    0    0    -1  
$EndComp
$Comp
L R R21
U 1 1 590C80D3
P 3400 2250
F 0 "R21" V 3300 2250 50  0000 C CNN
F 1 "0.27" V 3400 2250 50  0000 C CNN
F 2 "" V 3330 2250 50  0001 C CNN
F 3 "" H 3400 2250 50  0001 C CNN
	1    3400 2250
	0    1    1    0   
$EndComp
$Comp
L R R22
U 1 1 590C80D9
P 3850 2250
F 0 "R22" V 3750 2250 50  0000 C CNN
F 1 "0" V 3850 2250 50  0000 C CNN
F 2 "" V 3780 2250 50  0001 C CNN
F 3 "" H 3850 2250 50  0001 C CNN
	1    3850 2250
	0    1    1    0   
$EndComp
$Comp
L L L5
U 1 1 590C8114
P 1400 2750
F 0 "L5" V 1350 2750 50  0000 C CNN
F 1 "47uH" V 1475 2750 50  0000 C CNN
F 2 "" H 1400 2750 50  0001 C CNN
F 3 "" H 1400 2750 50  0001 C CNN
	1    1400 2750
	0    -1   -1   0   
$EndComp
$Comp
L C C19
U 1 1 590C811E
P 2800 3100
F 0 "C19" V 2900 3200 50  0000 L CNN
F 1 "1uF" V 2900 2900 50  0000 L CNN
F 2 "" H 2838 2950 50  0001 C CNN
F 3 "" H 2800 3100 50  0001 C CNN
	1    2800 3100
	0    1    1    0   
$EndComp
$Comp
L D VD5
U 1 1 590C8127
P 2350 1800
F 0 "VD5" H 2350 1900 50  0000 C CNN
F 1 "D" H 2350 1700 50  0000 C CNN
F 2 "" H 2350 1800 50  0001 C CNN
F 3 "" H 2350 1800 50  0001 C CNN
	1    2350 1800
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X05 XS1
U 1 1 590CB185
P 6100 7350
F 0 "XS1" H 6100 7650 50  0000 C CNN
F 1 "CONN_01X05" V 6200 7350 50  0000 C CNN
F 2 "" H 6100 7350 50  0001 C CNN
F 3 "" H 6100 7350 50  0001 C CNN
	1    6100 7350
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 590CE2B9
P 1900 6200
F 0 "C15" H 1925 6300 50  0000 L CNN
F 1 "1uF" H 1925 6100 50  0000 L CNN
F 2 "" H 1938 6050 50  0001 C CNN
F 3 "" H 1900 6200 50  0001 C CNN
	1    1900 6200
	1    0    0    -1  
$EndComp
$Comp
L C C14
U 1 1 590CE3BE
P 1600 6200
F 0 "C14" H 1625 6300 50  0000 L CNN
F 1 "1uF" H 1625 6100 50  0000 L CNN
F 2 "" H 1638 6050 50  0001 C CNN
F 3 "" H 1600 6200 50  0001 C CNN
	1    1600 6200
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 590CE499
P 1300 6200
F 0 "C13" H 1325 6300 50  0000 L CNN
F 1 "1uF" H 1325 6100 50  0000 L CNN
F 2 "" H 1338 6050 50  0001 C CNN
F 3 "" H 1300 6200 50  0001 C CNN
	1    1300 6200
	1    0    0    -1  
$EndComp
$Comp
L VAA #PWR7
U 1 1 590CF024
P 1300 5850
F 0 "#PWR7" H 1300 5700 50  0001 C CNN
F 1 "VAA" H 1300 6000 50  0000 C CNN
F 2 "" H 1300 5850 50  0001 C CNN
F 3 "" H 1300 5850 50  0001 C CNN
	1    1300 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR11
U 1 1 590CFAFA
P 1300 7100
F 0 "#PWR11" H 1300 6850 50  0001 C CNN
F 1 "GND" H 1300 6950 50  0000 C CNN
F 2 "" H 1300 7100 50  0001 C CNN
F 3 "" H 1300 7100 50  0001 C CNN
	1    1300 7100
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 590D14E7
P 3850 6200
F 0 "C6" H 3875 6300 50  0000 L CNN
F 1 "1uF" H 3875 6100 50  0000 L CNN
F 2 "" H 3888 6050 50  0001 C CNN
F 3 "" H 3850 6200 50  0001 C CNN
	1    3850 6200
	0    1    1    0   
$EndComp
$Comp
L L L3
U 1 1 590D1C31
P 4450 6200
F 0 "L3" V 4400 6200 50  0000 C CNN
F 1 "L" V 4525 6200 50  0000 C CNN
F 2 "" H 4450 6200 50  0001 C CNN
F 3 "" H 4450 6200 50  0001 C CNN
	1    4450 6200
	0    -1   -1   0   
$EndComp
$Comp
L D VD3
U 1 1 590D1E30
P 4200 6400
F 0 "VD3" H 4200 6500 50  0000 C CNN
F 1 "D" H 4200 6300 50  0000 C CNN
F 2 "" H 4200 6400 50  0001 C CNN
F 3 "" H 4200 6400 50  0001 C CNN
	1    4200 6400
	0    1    1    0   
$EndComp
$Comp
L C C8
U 1 1 590D2415
P 5500 6200
F 0 "C8" H 5525 6300 50  0000 L CNN
F 1 "1uF" H 5525 6100 50  0000 L CNN
F 2 "" H 5538 6050 50  0001 C CNN
F 3 "" H 5500 6200 50  0001 C CNN
	1    5500 6200
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 590D24F4
P 5750 6200
F 0 "C9" H 5775 6300 50  0000 L CNN
F 1 "1uF" H 5775 6100 50  0000 L CNN
F 2 "" H 5788 6050 50  0001 C CNN
F 3 "" H 5750 6200 50  0001 C CNN
	1    5750 6200
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 590D25DB
P 6000 6200
F 0 "C10" H 6025 6300 50  0000 L CNN
F 1 "1uF" H 6025 6100 50  0000 L CNN
F 2 "" H 6038 6050 50  0001 C CNN
F 3 "" H 6000 6200 50  0001 C CNN
	1    6000 6200
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 590D26C6
P 6250 6200
F 0 "C11" H 6275 6300 50  0000 L CNN
F 1 "1uF" H 6275 6100 50  0000 L CNN
F 2 "" H 6288 6050 50  0001 C CNN
F 3 "" H 6250 6200 50  0001 C CNN
	1    6250 6200
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 J2
U 1 1 590D43BD
P 7050 5950
F 0 "J2" H 7050 6100 50  0000 C CNN
F 1 "CONN_01X02" V 7150 5950 50  0000 C CNN
F 2 "" H 7050 5950 50  0001 C CNN
F 3 "" H 7050 5950 50  0001 C CNN
	1    7050 5950
	1    0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 590D6276
P 4850 6200
F 0 "R14" V 4930 6200 50  0000 C CNN
F 1 "14.7k" V 4850 6200 50  0000 C CNN
F 2 "" V 4780 6200 50  0001 C CNN
F 3 "" H 4850 6200 50  0001 C CNN
	1    4850 6200
	0    -1   -1   0   
$EndComp
$Comp
L R R13
U 1 1 590D6871
P 5050 6400
F 0 "R13" V 5130 6400 50  0000 C CNN
F 1 "1k" V 5050 6400 50  0000 C CNN
F 2 "" V 4980 6400 50  0001 C CNN
F 3 "" H 5050 6400 50  0001 C CNN
	1    5050 6400
	1    0    0    -1  
$EndComp
$Comp
L R R17
U 1 1 590D7596
P 1300 6850
F 0 "R17" V 1380 6850 50  0000 C CNN
F 1 "0" V 1300 6850 50  0000 C CNN
F 2 "" V 1230 6850 50  0001 C CNN
F 3 "" H 1300 6850 50  0001 C CNN
	1    1300 6850
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 590D8DAF
P 5400 7350
F 0 "R6" V 5480 7350 50  0000 C CNN
F 1 "0" V 5400 7350 50  0000 C CNN
F 2 "" V 5330 7350 50  0001 C CNN
F 3 "" H 5400 7350 50  0001 C CNN
	1    5400 7350
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 590D973C
P 5700 7450
F 0 "R7" V 5780 7450 50  0000 C CNN
F 1 "0" V 5700 7450 50  0000 C CNN
F 2 "" V 5630 7450 50  0001 C CNN
F 3 "" H 5700 7450 50  0001 C CNN
	1    5700 7450
	0    1    1    0   
$EndComp
$Comp
L al8807_sot DA3
U 1 1 5ACCC8BF
P 2350 2250
F 0 "DA3" H 2350 2150 50  0000 C CNN
F 1 "AL8807" H 2350 2350 50  0000 C CNN
F 2 "MODULE" H 2350 2250 50  0001 C CNN
F 3 "DOCUMENTATION" H 2350 2250 50  0001 C CNN
	1    2350 2250
	1    0    0    -1  
$EndComp
$Comp
L LED LED22
U 1 1 5ACD397F
P 3950 2750
F 0 "LED22" H 3950 2850 50  0000 C CNN
F 1 "LED" H 3950 2650 50  0000 C CNN
F 2 "" H 3950 2750 50  0001 C CNN
F 3 "" H 3950 2750 50  0001 C CNN
	1    3950 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED23
U 1 1 5ACD4034
P 3600 2750
F 0 "LED23" H 3600 2850 50  0000 C CNN
F 1 "LED" H 3600 2650 50  0000 C CNN
F 2 "" H 3600 2750 50  0001 C CNN
F 3 "" H 3600 2750 50  0001 C CNN
	1    3600 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED24
U 1 1 5ACD416C
P 3250 2750
F 0 "LED24" H 3250 2850 50  0000 C CNN
F 1 "LED" H 3250 2650 50  0000 C CNN
F 2 "" H 3250 2750 50  0001 C CNN
F 3 "" H 3250 2750 50  0001 C CNN
	1    3250 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED25
U 1 1 5ACD42A3
P 2900 2750
F 0 "LED25" H 2900 2850 50  0000 C CNN
F 1 "LED" H 2900 2650 50  0000 C CNN
F 2 "" H 2900 2750 50  0001 C CNN
F 3 "" H 2900 2750 50  0001 C CNN
	1    2900 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED26
U 1 1 5ACD43DD
P 2550 2750
F 0 "LED26" H 2550 2850 50  0000 C CNN
F 1 "LED" H 2550 2650 50  0000 C CNN
F 2 "" H 2550 2750 50  0001 C CNN
F 3 "" H 2550 2750 50  0001 C CNN
	1    2550 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED27
U 1 1 5ACD451A
P 2200 2750
F 0 "LED27" H 2200 2850 50  0000 C CNN
F 1 "LED" H 2200 2650 50  0000 C CNN
F 2 "" H 2200 2750 50  0001 C CNN
F 3 "" H 2200 2750 50  0001 C CNN
	1    2200 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED28
U 1 1 5ACD465A
P 1850 2750
F 0 "LED28" H 1850 2850 50  0000 C CNN
F 1 "LED" H 1850 2650 50  0000 C CNN
F 2 "" H 1850 2750 50  0001 C CNN
F 3 "" H 1850 2750 50  0001 C CNN
	1    1850 2750
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 5ACDC703
P 3450 1800
F 0 "C20" V 3550 1900 50  0000 L CNN
F 1 "1uF" V 3550 1600 50  0000 L CNN
F 2 "" H 3488 1650 50  0001 C CNN
F 3 "" H 3450 1800 50  0001 C CNN
	1    3450 1800
	0    1    1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACDD28F
P 1500 2400
F 0 "#PWR?" H 1500 2150 50  0001 C CNN
F 1 "GND" H 1500 2250 50  0000 C CNN
F 2 "" H 1500 2400 50  0001 C CNN
F 3 "" H 1500 2400 50  0001 C CNN
	1    1500 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACDDBFC
P 4000 1900
F 0 "#PWR?" H 4000 1650 50  0001 C CNN
F 1 "GND" H 4000 1750 50  0000 C CNN
F 2 "" H 4000 1900 50  0001 C CNN
F 3 "" H 4000 1900 50  0001 C CNN
	1    4000 1900
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 5ACE022B
P 4900 1550
F 0 "R19" V 4980 1550 50  0000 C CNN
F 1 "0" V 4900 1550 50  0000 C CNN
F 2 "" V 4830 1550 50  0001 C CNN
F 3 "" H 4900 1550 50  0001 C CNN
	1    4900 1550
	0    -1   -1   0   
$EndComp
$Comp
L R R18
U 1 1 5ACE0231
P 5250 1550
F 0 "R18" V 5150 1550 50  0000 C CNN
F 1 "0" V 5250 1550 50  0000 C CNN
F 2 "" V 5180 1550 50  0001 C CNN
F 3 "" H 5250 1550 50  0001 C CNN
	1    5250 1550
	0    1    1    0   
$EndComp
Text GLabel 5450 1550 2    60   Input ~ 0
ADJ
$Comp
L R R12
U 1 1 5ACE0238
P 6250 1550
F 0 "R12" V 6330 1550 50  0000 C CNN
F 1 "0" V 6250 1550 50  0000 C CNN
F 2 "" V 6180 1550 50  0001 C CNN
F 3 "" H 6250 1550 50  0001 C CNN
	1    6250 1550
	-1   0    0    1   
$EndComp
$Comp
L VAA #PWR?
U 1 1 5ACE023E
P 6250 1350
F 0 "#PWR?" H 6250 1200 50  0001 C CNN
F 1 "VAA" H 6250 1500 50  0000 C CNN
F 2 "" H 6250 1350 50  0001 C CNN
F 3 "" H 6250 1350 50  0001 C CNN
	1    6250 1350
	1    0    0    -1  
$EndComp
$Comp
L R R15
U 1 1 5ACE0244
P 6500 2250
F 0 "R15" V 6400 2250 50  0000 C CNN
F 1 "0.27" V 6500 2250 50  0000 C CNN
F 2 "" V 6430 2250 50  0001 C CNN
F 3 "" H 6500 2250 50  0001 C CNN
	1    6500 2250
	0    1    1    0   
$EndComp
$Comp
L R R16
U 1 1 5ACE024A
P 6950 2250
F 0 "R16" V 6850 2250 50  0000 C CNN
F 1 "0" V 6950 2250 50  0000 C CNN
F 2 "" V 6880 2250 50  0001 C CNN
F 3 "" H 6950 2250 50  0001 C CNN
	1    6950 2250
	0    1    1    0   
$EndComp
$Comp
L L L4
U 1 1 5ACE0250
P 4500 2750
F 0 "L4" V 4450 2750 50  0000 C CNN
F 1 "47uH" V 4575 2750 50  0000 C CNN
F 2 "" H 4500 2750 50  0001 C CNN
F 3 "" H 4500 2750 50  0001 C CNN
	1    4500 2750
	0    -1   -1   0   
$EndComp
$Comp
L C C12
U 1 1 5ACE0256
P 5900 3100
F 0 "C12" V 6000 3200 50  0000 L CNN
F 1 "1uF" V 6000 2900 50  0000 L CNN
F 2 "" H 5938 2950 50  0001 C CNN
F 3 "" H 5900 3100 50  0001 C CNN
	1    5900 3100
	0    1    1    0   
$EndComp
$Comp
L D VD4
U 1 1 5ACE025C
P 5450 1800
F 0 "VD4" H 5450 1900 50  0000 C CNN
F 1 "D" H 5450 1700 50  0000 C CNN
F 2 "" H 5450 1800 50  0001 C CNN
F 3 "" H 5450 1800 50  0001 C CNN
	1    5450 1800
	-1   0    0    1   
$EndComp
$Comp
L al8807_sot DA4
U 1 1 5ACE0263
P 5450 2250
F 0 "DA4" H 5450 2150 50  0000 C CNN
F 1 "AL8807" H 5450 2350 50  0000 C CNN
F 2 "MODULE" H 5450 2250 50  0001 C CNN
F 3 "DOCUMENTATION" H 5450 2250 50  0001 C CNN
	1    5450 2250
	1    0    0    -1  
$EndComp
$Comp
L LED LED15
U 1 1 5ACE026E
P 7050 2750
F 0 "LED15" H 7050 2850 50  0000 C CNN
F 1 "LED" H 7050 2650 50  0000 C CNN
F 2 "" H 7050 2750 50  0001 C CNN
F 3 "" H 7050 2750 50  0001 C CNN
	1    7050 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED16
U 1 1 5ACE0275
P 6700 2750
F 0 "LED16" H 6700 2850 50  0000 C CNN
F 1 "LED" H 6700 2650 50  0000 C CNN
F 2 "" H 6700 2750 50  0001 C CNN
F 3 "" H 6700 2750 50  0001 C CNN
	1    6700 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED17
U 1 1 5ACE027B
P 6350 2750
F 0 "LED17" H 6350 2850 50  0000 C CNN
F 1 "LED" H 6350 2650 50  0000 C CNN
F 2 "" H 6350 2750 50  0001 C CNN
F 3 "" H 6350 2750 50  0001 C CNN
	1    6350 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED18
U 1 1 5ACE0281
P 6000 2750
F 0 "LED18" H 6000 2850 50  0000 C CNN
F 1 "LED" H 6000 2650 50  0000 C CNN
F 2 "" H 6000 2750 50  0001 C CNN
F 3 "" H 6000 2750 50  0001 C CNN
	1    6000 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED19
U 1 1 5ACE0287
P 5650 2750
F 0 "LED19" H 5650 2850 50  0000 C CNN
F 1 "LED" H 5650 2650 50  0000 C CNN
F 2 "" H 5650 2750 50  0001 C CNN
F 3 "" H 5650 2750 50  0001 C CNN
	1    5650 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED20
U 1 1 5ACE028D
P 5300 2750
F 0 "LED20" H 5300 2850 50  0000 C CNN
F 1 "LED" H 5300 2650 50  0000 C CNN
F 2 "" H 5300 2750 50  0001 C CNN
F 3 "" H 5300 2750 50  0001 C CNN
	1    5300 2750
	1    0    0    -1  
$EndComp
$Comp
L LED LED21
U 1 1 5ACE0293
P 4950 2750
F 0 "LED21" H 4950 2850 50  0000 C CNN
F 1 "LED" H 4950 2650 50  0000 C CNN
F 2 "" H 4950 2750 50  0001 C CNN
F 3 "" H 4950 2750 50  0001 C CNN
	1    4950 2750
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 5ACE02AE
P 6550 1800
F 0 "C16" V 6650 1900 50  0000 L CNN
F 1 "1uF" V 6650 1600 50  0000 L CNN
F 2 "" H 6588 1650 50  0001 C CNN
F 3 "" H 6550 1800 50  0001 C CNN
	1    6550 1800
	0    1    1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACE02B4
P 4600 2400
F 0 "#PWR?" H 4600 2150 50  0001 C CNN
F 1 "GND" H 4600 2250 50  0000 C CNN
F 2 "" H 4600 2400 50  0001 C CNN
F 3 "" H 4600 2400 50  0001 C CNN
	1    4600 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACE02BC
P 7100 1900
F 0 "#PWR?" H 7100 1650 50  0001 C CNN
F 1 "GND" H 7100 1750 50  0000 C CNN
F 2 "" H 7100 1900 50  0001 C CNN
F 3 "" H 7100 1900 50  0001 C CNN
	1    7100 1900
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 5ACE0F9D
P 1800 3750
F 0 "R11" V 1880 3750 50  0000 C CNN
F 1 "0" V 1800 3750 50  0000 C CNN
F 2 "" V 1730 3750 50  0001 C CNN
F 3 "" H 1800 3750 50  0001 C CNN
	1    1800 3750
	0    -1   -1   0   
$EndComp
$Comp
L R R10
U 1 1 5ACE0FA3
P 2150 3750
F 0 "R10" V 2050 3750 50  0000 C CNN
F 1 "0" V 2150 3750 50  0000 C CNN
F 2 "" V 2080 3750 50  0001 C CNN
F 3 "" H 2150 3750 50  0001 C CNN
	1    2150 3750
	0    1    1    0   
$EndComp
Text GLabel 2350 3750 2    60   Input ~ 0
ADJ
$Comp
L VAA #PWR?
U 1 1 5ACE0FB0
P 3150 3550
F 0 "#PWR?" H 3150 3400 50  0001 C CNN
F 1 "VAA" H 3150 3700 50  0000 C CNN
F 2 "" H 3150 3550 50  0001 C CNN
F 3 "" H 3150 3550 50  0001 C CNN
	1    3150 3550
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 5ACE0FB6
P 3400 4450
F 0 "R8" V 3300 4450 50  0000 C CNN
F 1 "0.27" V 3400 4450 50  0000 C CNN
F 2 "" V 3330 4450 50  0001 C CNN
F 3 "" H 3400 4450 50  0001 C CNN
	1    3400 4450
	0    1    1    0   
$EndComp
$Comp
L R R9
U 1 1 5ACE0FBC
P 3850 4450
F 0 "R9" V 3750 4450 50  0000 C CNN
F 1 "0" V 3850 4450 50  0000 C CNN
F 2 "" V 3780 4450 50  0001 C CNN
F 3 "" H 3850 4450 50  0001 C CNN
	1    3850 4450
	0    1    1    0   
$EndComp
$Comp
L L L2
U 1 1 5ACE0FC2
P 1400 4950
F 0 "L2" V 1350 4950 50  0000 C CNN
F 1 "47uH" V 1475 4950 50  0000 C CNN
F 2 "" H 1400 4950 50  0001 C CNN
F 3 "" H 1400 4950 50  0001 C CNN
	1    1400 4950
	0    -1   -1   0   
$EndComp
$Comp
L C C4
U 1 1 5ACE0FC8
P 2800 5300
F 0 "C4" V 2900 5400 50  0000 L CNN
F 1 "1uF" V 2900 5100 50  0000 L CNN
F 2 "" H 2838 5150 50  0001 C CNN
F 3 "" H 2800 5300 50  0001 C CNN
	1    2800 5300
	0    1    1    0   
$EndComp
$Comp
L D VD2
U 1 1 5ACE0FCE
P 2350 4000
F 0 "VD2" H 2350 4100 50  0000 C CNN
F 1 "D" H 2350 3900 50  0000 C CNN
F 2 "" H 2350 4000 50  0001 C CNN
F 3 "" H 2350 4000 50  0001 C CNN
	1    2350 4000
	-1   0    0    1   
$EndComp
$Comp
L al8807_sot DA2
U 1 1 5ACE0FD5
P 2350 4450
F 0 "DA2" H 2350 4350 50  0000 C CNN
F 1 "AL8807" H 2350 4550 50  0000 C CNN
F 2 "MODULE" H 2350 4450 50  0001 C CNN
F 3 "DOCUMENTATION" H 2350 4450 50  0001 C CNN
	1    2350 4450
	1    0    0    -1  
$EndComp
$Comp
L LED LED8
U 1 1 5ACE0FE0
P 3950 4950
F 0 "LED8" H 3950 5050 50  0000 C CNN
F 1 "LED" H 3950 4850 50  0000 C CNN
F 2 "" H 3950 4950 50  0001 C CNN
F 3 "" H 3950 4950 50  0001 C CNN
	1    3950 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED9
U 1 1 5ACE0FE7
P 3600 4950
F 0 "LED9" H 3600 5050 50  0000 C CNN
F 1 "LED" H 3600 4850 50  0000 C CNN
F 2 "" H 3600 4950 50  0001 C CNN
F 3 "" H 3600 4950 50  0001 C CNN
	1    3600 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED10
U 1 1 5ACE0FED
P 3250 4950
F 0 "LED10" H 3250 5050 50  0000 C CNN
F 1 "LED" H 3250 4850 50  0000 C CNN
F 2 "" H 3250 4950 50  0001 C CNN
F 3 "" H 3250 4950 50  0001 C CNN
	1    3250 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED11
U 1 1 5ACE0FF3
P 2900 4950
F 0 "LED11" H 2900 5050 50  0000 C CNN
F 1 "LED" H 2900 4850 50  0000 C CNN
F 2 "" H 2900 4950 50  0001 C CNN
F 3 "" H 2900 4950 50  0001 C CNN
	1    2900 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED12
U 1 1 5ACE0FF9
P 2550 4950
F 0 "LED12" H 2550 5050 50  0000 C CNN
F 1 "LED" H 2550 4850 50  0000 C CNN
F 2 "" H 2550 4950 50  0001 C CNN
F 3 "" H 2550 4950 50  0001 C CNN
	1    2550 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED13
U 1 1 5ACE0FFF
P 2200 4950
F 0 "LED13" H 2200 5050 50  0000 C CNN
F 1 "LED" H 2200 4850 50  0000 C CNN
F 2 "" H 2200 4950 50  0001 C CNN
F 3 "" H 2200 4950 50  0001 C CNN
	1    2200 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED14
U 1 1 5ACE1005
P 1850 4950
F 0 "LED14" H 1850 5050 50  0000 C CNN
F 1 "LED" H 1850 4850 50  0000 C CNN
F 2 "" H 1850 4950 50  0001 C CNN
F 3 "" H 1850 4950 50  0001 C CNN
	1    1850 4950
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5ACE1020
P 3450 4000
F 0 "C5" V 3550 4100 50  0000 L CNN
F 1 "1uF" V 3550 3800 50  0000 L CNN
F 2 "" H 3488 3850 50  0001 C CNN
F 3 "" H 3450 4000 50  0001 C CNN
	1    3450 4000
	0    1    1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACE1026
P 1500 4600
F 0 "#PWR?" H 1500 4350 50  0001 C CNN
F 1 "GND" H 1500 4450 50  0000 C CNN
F 2 "" H 1500 4600 50  0001 C CNN
F 3 "" H 1500 4600 50  0001 C CNN
	1    1500 4600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACE102E
P 4000 4100
F 0 "#PWR?" H 4000 3850 50  0001 C CNN
F 1 "GND" H 4000 3950 50  0000 C CNN
F 2 "" H 4000 4100 50  0001 C CNN
F 3 "" H 4000 4100 50  0001 C CNN
	1    4000 4100
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5ACE103F
P 4900 3750
F 0 "R5" V 4980 3750 50  0000 C CNN
F 1 "0" V 4900 3750 50  0000 C CNN
F 2 "" V 4830 3750 50  0001 C CNN
F 3 "" H 4900 3750 50  0001 C CNN
	1    4900 3750
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 5ACE1045
P 5250 3750
F 0 "R4" V 5150 3750 50  0000 C CNN
F 1 "0" V 5250 3750 50  0000 C CNN
F 2 "" V 5180 3750 50  0001 C CNN
F 3 "" H 5250 3750 50  0001 C CNN
	1    5250 3750
	0    1    1    0   
$EndComp
Text GLabel 5450 3750 2    60   Input ~ 0
ADJ
$Comp
L R R1
U 1 1 5ACE104C
P 6250 3750
F 0 "R1" V 6330 3750 50  0000 C CNN
F 1 "0" V 6250 3750 50  0000 C CNN
F 2 "" V 6180 3750 50  0001 C CNN
F 3 "" H 6250 3750 50  0001 C CNN
	1    6250 3750
	-1   0    0    1   
$EndComp
$Comp
L VAA #PWR?
U 1 1 5ACE1052
P 6250 3550
F 0 "#PWR?" H 6250 3400 50  0001 C CNN
F 1 "VAA" H 6250 3700 50  0000 C CNN
F 2 "" H 6250 3550 50  0001 C CNN
F 3 "" H 6250 3550 50  0001 C CNN
	1    6250 3550
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5ACE1058
P 6500 4450
F 0 "R2" V 6400 4450 50  0000 C CNN
F 1 "0.27" V 6500 4450 50  0000 C CNN
F 2 "" V 6430 4450 50  0001 C CNN
F 3 "" H 6500 4450 50  0001 C CNN
	1    6500 4450
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 5ACE105E
P 6950 4450
F 0 "R3" V 6850 4450 50  0000 C CNN
F 1 "0" V 6950 4450 50  0000 C CNN
F 2 "" V 6880 4450 50  0001 C CNN
F 3 "" H 6950 4450 50  0001 C CNN
	1    6950 4450
	0    1    1    0   
$EndComp
$Comp
L L L1
U 1 1 5ACE1064
P 4500 4950
F 0 "L1" V 4450 4950 50  0000 C CNN
F 1 "47uH" V 4575 4950 50  0000 C CNN
F 2 "" H 4500 4950 50  0001 C CNN
F 3 "" H 4500 4950 50  0001 C CNN
	1    4500 4950
	0    -1   -1   0   
$EndComp
$Comp
L C C1
U 1 1 5ACE106A
P 5900 5300
F 0 "C1" V 6000 5400 50  0000 L CNN
F 1 "1uF" V 6000 5100 50  0000 L CNN
F 2 "" H 5938 5150 50  0001 C CNN
F 3 "" H 5900 5300 50  0001 C CNN
	1    5900 5300
	0    1    1    0   
$EndComp
$Comp
L D VD1
U 1 1 5ACE1070
P 5450 4000
F 0 "VD1" H 5450 4100 50  0000 C CNN
F 1 "D" H 5450 3900 50  0000 C CNN
F 2 "" H 5450 4000 50  0001 C CNN
F 3 "" H 5450 4000 50  0001 C CNN
	1    5450 4000
	-1   0    0    1   
$EndComp
$Comp
L al8807_sot DA1
U 1 1 5ACE1077
P 5450 4450
F 0 "DA1" H 5450 4350 50  0000 C CNN
F 1 "AL8807" H 5450 4550 50  0000 C CNN
F 2 "MODULE" H 5450 4450 50  0001 C CNN
F 3 "DOCUMENTATION" H 5450 4450 50  0001 C CNN
	1    5450 4450
	1    0    0    -1  
$EndComp
$Comp
L LED LED1
U 1 1 5ACE1082
P 7050 4950
F 0 "LED1" H 7050 5050 50  0000 C CNN
F 1 "LED" H 7050 4850 50  0000 C CNN
F 2 "" H 7050 4950 50  0001 C CNN
F 3 "" H 7050 4950 50  0001 C CNN
	1    7050 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED2
U 1 1 5ACE1089
P 6700 4950
F 0 "LED2" H 6700 5050 50  0000 C CNN
F 1 "LED" H 6700 4850 50  0000 C CNN
F 2 "" H 6700 4950 50  0001 C CNN
F 3 "" H 6700 4950 50  0001 C CNN
	1    6700 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED3
U 1 1 5ACE108F
P 6350 4950
F 0 "LED3" H 6350 5050 50  0000 C CNN
F 1 "LED" H 6350 4850 50  0000 C CNN
F 2 "" H 6350 4950 50  0001 C CNN
F 3 "" H 6350 4950 50  0001 C CNN
	1    6350 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED4
U 1 1 5ACE1095
P 6000 4950
F 0 "LED4" H 6000 5050 50  0000 C CNN
F 1 "LED" H 6000 4850 50  0000 C CNN
F 2 "" H 6000 4950 50  0001 C CNN
F 3 "" H 6000 4950 50  0001 C CNN
	1    6000 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED5
U 1 1 5ACE109B
P 5650 4950
F 0 "LED5" H 5650 5050 50  0000 C CNN
F 1 "LED" H 5650 4850 50  0000 C CNN
F 2 "" H 5650 4950 50  0001 C CNN
F 3 "" H 5650 4950 50  0001 C CNN
	1    5650 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED6
U 1 1 5ACE10A1
P 5300 4950
F 0 "LED6" H 5300 5050 50  0000 C CNN
F 1 "LED" H 5300 4850 50  0000 C CNN
F 2 "" H 5300 4950 50  0001 C CNN
F 3 "" H 5300 4950 50  0001 C CNN
	1    5300 4950
	1    0    0    -1  
$EndComp
$Comp
L LED LED7
U 1 1 5ACE10A7
P 4950 4950
F 0 "LED7" H 4950 5050 50  0000 C CNN
F 1 "LED" H 4950 4850 50  0000 C CNN
F 2 "" H 4950 4950 50  0001 C CNN
F 3 "" H 4950 4950 50  0001 C CNN
	1    4950 4950
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5ACE10C2
P 6550 4000
F 0 "C2" V 6650 4100 50  0000 L CNN
F 1 "1uF" V 6650 3800 50  0000 L CNN
F 2 "" H 6588 3850 50  0001 C CNN
F 3 "" H 6550 4000 50  0001 C CNN
	1    6550 4000
	0    1    1    0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACE10C8
P 4600 4600
F 0 "#PWR?" H 4600 4350 50  0001 C CNN
F 1 "GND" H 4600 4450 50  0000 C CNN
F 2 "" H 4600 4600 50  0001 C CNN
F 3 "" H 4600 4600 50  0001 C CNN
	1    4600 4600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5ACE10D0
P 7100 4100
F 0 "#PWR?" H 7100 3850 50  0001 C CNN
F 1 "GND" H 7100 3950 50  0000 C CNN
F 2 "" H 7100 4100 50  0001 C CNN
F 3 "" H 7100 4100 50  0001 C CNN
	1    7100 4100
	1    0    0    -1  
$EndComp
$Comp
L LMR14203 DA3
U 1 1 5ACF05DE
P 2950 6100
F 0 "DA3" H 2950 6350 50  0000 C CNN
F 1 "LMR14203" H 2950 5850 50  0000 C CNN
F 2 "MODULE" H 2950 6100 50  0001 C CNN
F 3 "DOCUMENTATION" H 2950 6100 50  0001 C CNN
	1    2950 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 7150 5900 7150
Wire Wire Line
	4500 7250 5900 7250
Wire Wire Line
	4500 7350 5250 7350
Wire Wire Line
	4500 7450 5550 7450
Wire Wire Line
	4850 7450 4850 7550
Wire Wire Line
	4850 7150 4850 7050
Wire Wire Line
	3150 1400 3150 1350
Connection ~ 4850 7150
Wire Wire Line
	5200 7650 5200 7250
Connection ~ 5200 7250
Connection ~ 4850 7450
Wire Wire Line
	1300 6000 2300 6000
Wire Wire Line
	1900 6050 1900 6000
Wire Wire Line
	1600 6000 1600 6050
Wire Wire Line
	1300 5850 1300 6050
Connection ~ 1600 6000
Connection ~ 1300 6000
Wire Wire Line
	4650 5900 6850 5900
Wire Wire Line
	6250 5900 6250 6050
Wire Wire Line
	5500 6050 5500 5900
Connection ~ 5500 5900
Wire Wire Line
	5750 6050 5750 5900
Connection ~ 5750 5900
Wire Wire Line
	6000 6050 6000 5900
Connection ~ 6000 5900
Wire Wire Line
	5500 6600 5500 6350
Connection ~ 6250 5900
Wire Wire Line
	6800 6600 6800 6000
Wire Wire Line
	6800 6000 6850 6000
Wire Wire Line
	1300 7000 1300 7100
Wire Wire Line
	5550 7350 5900 7350
Wire Wire Line
	5850 7450 5900 7450
Wire Wire Line
	3100 2250 3250 2250
Wire Wire Line
	3550 2250 3700 2250
Wire Wire Line
	3100 2350 4100 2350
Wire Wire Line
	4100 2250 4100 3100
Wire Wire Line
	4100 2250 4000 2250
Connection ~ 4100 2350
Wire Wire Line
	3750 2750 3800 2750
Wire Wire Line
	3450 2750 3400 2750
Wire Wire Line
	3100 2750 3050 2750
Wire Wire Line
	2750 2750 2700 2750
Wire Wire Line
	2400 2750 2350 2750
Wire Wire Line
	2050 2750 2000 2750
Wire Wire Line
	4100 3100 2950 3100
Connection ~ 4100 2750
Wire Wire Line
	1550 2750 1700 2750
Wire Wire Line
	1650 2750 1650 3100
Wire Wire Line
	1650 3100 2650 3100
Connection ~ 1650 2750
Wire Wire Line
	1200 2750 1250 2750
Wire Wire Line
	1200 1800 1200 2750
Wire Wire Line
	1200 2150 1600 2150
Wire Wire Line
	3150 1700 3150 2250
Connection ~ 3150 2250
Wire Wire Line
	2500 1800 3300 1800
Connection ~ 3150 1800
Wire Wire Line
	2200 1800 1200 1800
Connection ~ 1200 2150
Wire Wire Line
	1500 2400 1500 2250
Wire Wire Line
	1500 2250 1600 2250
Wire Wire Line
	3600 1800 4000 1800
Wire Wire Line
	4000 1800 4000 1900
Wire Wire Line
	2300 1550 2350 1550
Wire Wire Line
	2000 1550 1950 1550
Wire Wire Line
	1600 2350 1550 2350
Wire Wire Line
	1550 2350 1550 1550
Wire Wire Line
	1550 1550 1650 1550
Wire Notes Line
	1100 1100 1100 8000
Wire Notes Line
	1100 1100 7300 1100
Wire Notes Line
	4200 1100 4200 5500
Wire Notes Line
	1100 3300 7300 3300
Wire Wire Line
	6250 1400 6250 1350
Wire Wire Line
	6200 2250 6350 2250
Wire Wire Line
	6650 2250 6800 2250
Wire Wire Line
	6200 2350 7200 2350
Wire Wire Line
	7200 2250 7200 3100
Wire Wire Line
	7200 2250 7100 2250
Connection ~ 7200 2350
Wire Wire Line
	6850 2750 6900 2750
Wire Wire Line
	6550 2750 6500 2750
Wire Wire Line
	6200 2750 6150 2750
Wire Wire Line
	5850 2750 5800 2750
Wire Wire Line
	5500 2750 5450 2750
Wire Wire Line
	5150 2750 5100 2750
Wire Wire Line
	7200 3100 6050 3100
Connection ~ 7200 2750
Wire Wire Line
	4650 2750 4800 2750
Wire Wire Line
	4750 2750 4750 3100
Wire Wire Line
	4750 3100 5750 3100
Connection ~ 4750 2750
Wire Wire Line
	4300 2750 4350 2750
Wire Wire Line
	4300 1800 4300 2750
Wire Wire Line
	4300 2150 4700 2150
Wire Wire Line
	6250 1700 6250 2250
Connection ~ 6250 2250
Wire Wire Line
	5600 1800 6400 1800
Connection ~ 6250 1800
Wire Wire Line
	5300 1800 4300 1800
Connection ~ 4300 2150
Wire Wire Line
	4600 2400 4600 2250
Wire Wire Line
	4600 2250 4700 2250
Wire Wire Line
	6700 1800 7100 1800
Wire Wire Line
	7100 1800 7100 1900
Wire Wire Line
	5400 1550 5450 1550
Wire Wire Line
	5100 1550 5050 1550
Wire Wire Line
	4700 2350 4650 2350
Wire Wire Line
	4650 2350 4650 1550
Wire Wire Line
	4650 1550 4750 1550
Wire Notes Line
	7300 1100 7300 8000
Wire Wire Line
	3150 3550 3150 4450
Wire Wire Line
	3100 4450 3250 4450
Wire Wire Line
	3550 4450 3700 4450
Wire Wire Line
	3100 4550 4100 4550
Wire Wire Line
	4100 4450 4100 5300
Wire Wire Line
	4100 4450 4000 4450
Connection ~ 4100 4550
Wire Wire Line
	3750 4950 3800 4950
Wire Wire Line
	3450 4950 3400 4950
Wire Wire Line
	3100 4950 3050 4950
Wire Wire Line
	2750 4950 2700 4950
Wire Wire Line
	2400 4950 2350 4950
Wire Wire Line
	2050 4950 2000 4950
Wire Wire Line
	4100 5300 2950 5300
Connection ~ 4100 4950
Wire Wire Line
	1550 4950 1700 4950
Wire Wire Line
	1650 4950 1650 5300
Wire Wire Line
	1650 5300 2650 5300
Connection ~ 1650 4950
Wire Wire Line
	1200 4950 1250 4950
Wire Wire Line
	1200 4000 1200 4950
Wire Wire Line
	1200 4350 1600 4350
Connection ~ 3150 4450
Wire Wire Line
	2500 4000 3300 4000
Connection ~ 3150 4000
Wire Wire Line
	2200 4000 1200 4000
Connection ~ 1200 4350
Wire Wire Line
	1500 4600 1500 4450
Wire Wire Line
	1500 4450 1600 4450
Wire Wire Line
	3600 4000 4000 4000
Wire Wire Line
	4000 4000 4000 4100
Wire Wire Line
	2300 3750 2350 3750
Wire Wire Line
	2000 3750 1950 3750
Wire Wire Line
	1600 4550 1550 4550
Wire Wire Line
	1550 4550 1550 3750
Wire Wire Line
	1550 3750 1650 3750
Wire Notes Line
	7300 5500 1100 5500
Wire Wire Line
	6250 3600 6250 3550
Wire Wire Line
	6200 4450 6350 4450
Wire Wire Line
	6650 4450 6800 4450
Wire Wire Line
	6200 4550 7200 4550
Wire Wire Line
	7200 4450 7200 5300
Wire Wire Line
	7200 4450 7100 4450
Connection ~ 7200 4550
Wire Wire Line
	6850 4950 6900 4950
Wire Wire Line
	6550 4950 6500 4950
Wire Wire Line
	6200 4950 6150 4950
Wire Wire Line
	5850 4950 5800 4950
Wire Wire Line
	5500 4950 5450 4950
Wire Wire Line
	5150 4950 5100 4950
Wire Wire Line
	7200 5300 6050 5300
Connection ~ 7200 4950
Wire Wire Line
	4650 4950 4800 4950
Wire Wire Line
	4750 4950 4750 5300
Wire Wire Line
	4750 5300 5750 5300
Connection ~ 4750 4950
Wire Wire Line
	4300 4950 4350 4950
Wire Wire Line
	4300 4000 4300 4950
Wire Wire Line
	4300 4350 4700 4350
Wire Wire Line
	6250 3900 6250 4450
Connection ~ 6250 4450
Wire Wire Line
	5600 4000 6400 4000
Connection ~ 6250 4000
Wire Wire Line
	5300 4000 4300 4000
Connection ~ 4300 4350
Wire Wire Line
	4600 4600 4600 4450
Wire Wire Line
	4600 4450 4700 4450
Wire Wire Line
	6700 4000 7100 4000
Wire Wire Line
	7100 4000 7100 4100
Wire Wire Line
	5400 3750 5450 3750
Wire Wire Line
	5100 3750 5050 3750
Wire Wire Line
	4700 4550 4650 4550
Wire Wire Line
	4650 4550 4650 3750
Wire Wire Line
	4650 3750 4750 3750
Connection ~ 1900 6000
Wire Wire Line
	1300 6350 1300 6700
Wire Wire Line
	2300 6100 2200 6100
Wire Wire Line
	2200 6100 2200 6000
Connection ~ 2200 6000
Wire Wire Line
	3600 6200 3700 6200
Wire Wire Line
	4000 6200 4300 6200
Wire Wire Line
	4050 6200 4050 6450
Wire Wire Line
	4050 6450 2200 6450
Wire Wire Line
	2200 6450 2200 6200
Wire Wire Line
	2200 6200 2300 6200
Wire Wire Line
	4200 6200 4200 6250
Connection ~ 4050 6200
Wire Wire Line
	1300 6600 6800 6600
Wire Wire Line
	1600 6350 1600 6600
Connection ~ 1600 6600
Wire Wire Line
	1900 6350 1900 6600
Connection ~ 1900 6600
Wire Wire Line
	3600 6100 3650 6100
Wire Wire Line
	3650 6100 3650 6600
Wire Wire Line
	4200 6600 4200 6550
Connection ~ 3650 6600
Connection ~ 4200 6200
Connection ~ 1300 6600
Wire Wire Line
	5000 6200 5050 6200
Wire Wire Line
	5050 6000 5050 6250
Wire Wire Line
	5050 6000 3600 6000
Wire Wire Line
	5050 6600 5050 6550
Connection ~ 4200 6600
Connection ~ 5050 6200
Wire Wire Line
	4600 6200 4700 6200
Wire Wire Line
	4650 6200 4650 5900
Connection ~ 4650 6200
Connection ~ 5050 6600
Wire Wire Line
	5750 6600 5750 6350
Connection ~ 5500 6600
Wire Wire Line
	6000 6600 6000 6350
Connection ~ 5750 6600
Wire Wire Line
	6250 6600 6250 6350
Connection ~ 6000 6600
Connection ~ 6250 6600
Wire Notes Line
	7300 8000 1100 8000
$EndSCHEMATC
