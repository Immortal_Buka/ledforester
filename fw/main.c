#include <project.h>
#include <ble_main.h>
#include <stdio.h>
//
#define MUSIC_COUNTER   8
//
typedef struct 
{
    uint8_t repeat;
    uint8_t lenght;
    const uint16_t* pointer;
} music_descriptor;
//(clk/prescaler)/note_freq => 12M/16=750k	
const uint16_t music_1[4] = {5734,2700,0,0};//device on
const uint16_t music_2[5] = {1434,0,0,0,0};//work light warning (every 4 sec)
const uint16_t music_3[3] = {4054,0,0};//PWM settings received & work light end
const uint16_t music_4[3] = {852,0,0};//bonding change
const uint16_t music_5[3] = {1434,0,0};//sound volume test
const uint16_t music_6[3] = {4054,0,0};//no bluetooth connection (every 2 sec)
const uint16_t music_7[3] = {11466,0,0};//
const uint16_t music_8[1] = {0};//off
const music_descriptor descriptors[MUSIC_COUNTER] = 
{
    {0,4,music_1},
    {1,5,music_2},
    {0,3,music_3},
    {0,3,music_4},
    {0,3,music_5},
    {1,3,music_6},
    {0,3,music_7},
    {0,1,music_8}
};
uint8_t music_number = 0, music_work = 0, status = 0;
uint16_t music_counter = 0, pwm_mult = 0;
char dev_name[] = "LEDforester_0000";
const char string_info[] =      "\r\n\e[34m[INFO]\e[39m ";//blue
const char string_ok[] =        "\r\n\e[32m[ OK ]\e[39m ";//green
const char string_fail[] =      "\r\n\e[31m[FAIL]\e[39m ";//red
const char string_ble_evt[] =   "\r\n\e[36m[EVTB]\e[39m ";//cyan
const char string_irq[] =       "\r\n\e[35m[IRQ ]\e[39m ";//margenta
//
uint32_t passkey;
CYBLE_CONN_HANDLE_T connectionHandle;
uint8_t temp;
CYBLE_STACK_LIB_VERSION_T ble_ver;
CYBLE_API_RESULT_T result;
CYBLE_GAP_BONDED_DEV_ADDR_LIST_T bondedDeviceList;
uint8_t bondedDeviceKeysFlag;
CYBLE_GAP_SMP_KEY_DIST_T securityKeys;
//
CYBLE_GATT_ERR_CODE_T update_characteristic_byte(uint8* data, uint16 attrHandle);
CYBLE_API_RESULT_T notify_characteristic_byte(uint8* data, uint16 attrHandle);
void ble_event_handler(uint32 event, void* eventParam);
CY_ISR(halfsec_irq_handler);
CY_ISR(slave_irq_handler);
void board_init(void);
void conn_led_on(void);
void conn_led_off(void);
void signal_led_on(void);
void signal_led_off(void);
void debug_led_on(void);
void debug_led_off(void);
void main(void);
void print_auth_info(CYBLE_GAP_AUTH_INFO_T* pointer);
void print_conn_param(CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T* pointer);
void print_6dec(uint32_t data);
void print_hci_error(CYBLE_HCI_ERROR_T data);
void print_2hex(uint8_t data);
void slave_chk(void);
void print_function_state(char* name, CYBLE_API_RESULT_T* result);
void print_address(uint8_t* addr);
void key_info_print(CYBLE_GAP_SMP_KEY_DIST_T* ptr);
//
CYBLE_GATT_ERR_CODE_T update_characteristic_byte(uint8* data, uint16 attrHandle)
{
	CYBLE_GATT_HANDLE_VALUE_PAIR_T handle;
    CYBLE_GATT_ERR_CODE_T result;
	handle.attrHandle = attrHandle;
	handle.value.val = data;
	handle.value.len = 1;
	result = CyBle_GattsWriteAttributeValue(&handle, 0, &connectionHandle, CYBLE_GATT_DB_LOCALLY_INITIATED);
    return result;
}
CYBLE_API_RESULT_T notify_characteristic_byte(uint8* data, uint16 attrHandle)
{
    CYBLE_GATTS_HANDLE_VALUE_NTF_T handle;
    CYBLE_API_RESULT_T result;
    handle.value.val  = data;
    handle.value.len  = 1;
    handle.attrHandle = attrHandle;
    result = CyBle_GattsNotification(connectionHandle, &handle);
    return result;
}
void ble_event_handler(uint32 event, void* eventParam)
{
    uint8_t data;
    uart_debug_UartPutString(string_ble_evt);
	switch(event)
    {
		case CYBLE_EVT_STACK_ON:
            uart_debug_UartPutString("bluetooth stack ON");
            result = CyBle_GapFixAuthPassKey(1,passkey);
            print_function_state("CyBle_GapFixAuthPassKey", &result);
            result = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            print_function_state("CyBle_GappStartAdvertisement", &result);
            if(update_characteristic_byte(&status, CYBLE_TEST_SERVICE_STATUS_CHAR_HANDLE) == CYBLE_GATT_ERR_NONE) 
                uart_debug_UartPutString(string_ok);
            else uart_debug_UartPutString(string_fail);
            uart_debug_UartPutString(" update_characteristic_byte");
            break;
        case CYBLE_EVT_GAP_SMP_NEGOTIATED_AUTH_INFO:
            uart_debug_UartPutString("SMP has completed pairing properties negotiation");
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            break;
        case CYBLE_EVT_TIMEOUT:
            uart_debug_UartPutString("is a timeout and application needs to handle the event");
            uart_debug_UartPutString(string_info);
            uart_debug_UartPutString("Timeout reason: ");
            switch((CYBLE_TO_REASON_CODE_T)eventParam)
            {
                case CYBLE_GAP_ADV_MODE_TO: uart_debug_UartPutString("Advertisement time set by application has expired"); break;
                case CYBLE_GAP_SCAN_TO: uart_debug_UartPutString("Scan time set by application has expired"); break;
                case CYBLE_GATT_RSP_TO: uart_debug_UartPutString("GATT procedure timeout"); break;
                case CYBLE_GENERIC_TO: uart_debug_UartPutString("Generic timeout"); break;
                default: uart_debug_UartPutString("unknown"); break;
            }
            break;
        case CYBLE_EVT_GAP_KEYINFO_EXCHNGE_CMPLT:
            uart_debug_UartPutString("SMP keys exchange with peer device is complete");
            key_info_print((CYBLE_GAP_SMP_KEY_DIST_T*)eventParam);
            break;  
        case CYBLE_EVT_PENDING_FLASH_WRITE:
            uart_debug_UartPutString("flash write is pending");
            break;
        case CYBLE_EVT_FLASH_CORRUPT:
            uart_debug_UartPutString("bonding information stored in flash is corrupted");
            break;
        case CYBLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE:
            uart_debug_UartPutString("gap connection update complete");
            print_conn_param((CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T*)eventParam);
            break;
        case CYBLE_EVT_GATTS_XCNHG_MTU_REQ:
            uart_debug_UartPutString("gatt mtu exchange request: ");            
            print_6dec(((CYBLE_GATT_XCHG_MTU_PARAM_T *)eventParam)->mtu);
            break;            
        case CYBLE_EVT_GAPP_ADVERTISEMENT_START_STOP:
            if(CyBle_GetState() == CYBLE_STATE_ADVERTISING) uart_debug_UartPutString("advertising start");                    
            else uart_debug_UartPutString("advertising stop");
            break;            
        case CYBLE_EVT_GAP_DEVICE_CONNECTED:
            uart_debug_UartPutString("gap device connected");
            result = CyBle_GapAuthReq(connectionHandle.bdHandle, &cyBle_authInfo);
            print_function_state("CyBle_GapAuthReq", &result);
            print_conn_param((CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T*)eventParam);
            break;
        case CYBLE_EVT_GAP_AUTH_REQ:
            uart_debug_UartPutString("gap authentication request");
            /*result = CyBle_GappAuthReqReply(connectionHandle.bdHandle,&cyBle_authInfo);
            print_function_state("CyBle_GappAuthReqReply", &result);*/
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            break; 
        case CYBLE_EVT_GAP_PASSKEY_DISPLAY_REQUEST:
            uart_debug_UartPutString("gap passkey display request");
            //print_6dec(*((uint32_t*)eventParam));//pass
            break;  
        case CYBLE_EVT_GAP_AUTH_COMPLETE:
            uart_debug_UartPutString("gap authentication OK");
            print_auth_info((CYBLE_GAP_AUTH_INFO_T*)eventParam);
            music_number = 8;//turn off sound
            conn_led_on();
            break;
        case CYBLE_EVT_GAP_ENCRYPT_CHANGE:
            switch(*(uint8 *)eventParam)
            {
                case 0: uart_debug_UartPutString("encrytpion OFF"); break;
                case 1: uart_debug_UartPutString("encrytpion ON"); break;
                default: uart_debug_UartPutString("encrytpion \e[31mERROR\e[39m"); break;
            }
            break; 
        case CYBLE_EVT_GAP_AUTH_FAILED:
            uart_debug_UartPutString("gap authentication \e[31mFAIL\e[39m");
			uart_debug_UartPutString(string_info);
            switch(*(CYBLE_GAP_AUTH_FAILED_REASON_T *)eventParam)
            {
                case CYBLE_GAP_AUTH_ERROR_PASSKEY_ENTRY_FAILED:
                    uart_debug_UartPutString("user input of passkey failed");
                    break;
                case CYBLE_GAP_AUTH_ERROR_REPEATED_ATTEMPTS:
                    uart_debug_UartPutString("too little time has elapsed since last request");
                    break;
                case CYBLE_GAP_AUTH_ERROR_CONFIRM_VALUE_NOT_MATCH:
                    uart_debug_UartPutString("confirm value does not match the calculated compare value");
                    break;
                case CYBLE_GAP_AUTH_ERROR_LINK_DISCONNECTED:
                    uart_debug_UartPutString("link disconnected");
                    break;    
                case CYBLE_GAP_AUTH_ERROR_AUTHENTICATION_TIMEOUT:
                    uart_debug_UartPutString("authentication process timeout");
                    break;     
                case CYBLE_GAP_AUTH_ERROR_UNSPECIFIED_REASON:
                    uart_debug_UartPutString("pairing failed due to an unspecified reason");
                    break;
                case CYBLE_GAP_AUTH_ERROR_INSUFFICIENT_ENCRYPTION_KEY_SIZE:
                    uart_debug_UartPutString("insufficient key size for the security requirements of this device or LTK is lost");    
                    break;
                case CYBLE_GAP_AUTH_ERROR_AUTHENTICATION_REQ_NOT_MET:
                    uart_debug_UartPutString("pairing procedure cannot be performed as authentication requirements cannot be met due to IO capabilities of one or both devices");
                    break; 
                default:
                	print_6dec(*(CYBLE_GAP_AUTH_FAILED_REASON_T *)eventParam);
                    break;
            }
            result = CyBle_GapDisconnect(connectionHandle.bdHandle);
            print_function_state("CyBle_GapDisconnect", &result);
            break;             
        case CYBLE_EVT_GATT_CONNECT_IND:
            uart_debug_UartPutString("gatt connect");
            connectionHandle = *(CYBLE_CONN_HANDLE_T *)eventParam;
            break;
        case CYBLE_EVT_HCI_STATUS:
            uart_debug_UartPutString("hci \e[31mERROR\e[39m");
			uart_debug_UartPutString(string_info);
            print_hci_error(*((CYBLE_HCI_ERROR_T*)eventParam));
            break;             
        case CYBLE_EVT_GAP_DEVICE_DISCONNECTED:
            uart_debug_UartPutString("gap device disconnected");
			uart_debug_UartPutString(string_info);
            print_hci_error(*((CYBLE_HCI_ERROR_T*)eventParam));
            connectionHandle.bdHandle=0;
            result = CyBle_GappStartAdvertisement(CYBLE_ADVERTISING_FAST);
            print_function_state("CyBle_GappStartAdvertisement", &result);
            break;    
        case CYBLE_EVT_GATTS_WRITE_REQ:
            uart_debug_UartPutString("gatts write request");
			uart_debug_UartPutString(string_info);
            data = *((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.value.val;
            switch(((CYBLE_GATTS_WRITE_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle)
            {
                case CYBLE_TEST_SERVICE_VOLUME_CHAR_HANDLE:
                    uart_debug_UartPutString("set volume: ");
                    break;
                case CYBLE_TEST_SERVICE_POWER_CHAR_HANDLE:
                    uart_debug_UartPutString("set pwm_1: ");
                    break;
                case CYBLE_TEST_SERVICE_POWER_2_CHAR_HANDLE:
                    uart_debug_UartPutString("set pwm_2: ");
                    break;                    
                case CYBLE_TEST_SERVICE_CONTROL_CHAR_HANDLE:
                    uart_debug_UartPutString("control: ");
                    break;
                case CYBLE_TEST_SERVICE_STATUS_CHAR_HANDLE:
                    uart_debug_UartPutString("set status: ");
                    break;    
                default:
                    uart_debug_UartPutString("unknown: ");
                    break;
            }
            print_6dec(data);
            CyBle_GattsWriteRsp(connectionHandle);
            break;
        case CYBLE_EVT_GATTS_WRITE_CMD_REQ:
            uart_debug_UartPutString("gatts write command");
			uart_debug_UartPutString(string_info);
            if(CyBle_GattsWriteAttributeValue
            	(
            		&((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair,
					0,
					&((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->connHandle,
					CYBLE_GATT_DB_PEER_INITIATED
            	)
			== CYBLE_GATT_ERR_NONE)
            {
                data = *((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair.value.val;
                switch(((CYBLE_GATTS_WRITE_CMD_REQ_PARAM_T*)eventParam)->handleValPair.attrHandle)
                {
                    case CYBLE_TEST_SERVICE_VOLUME_CHAR_HANDLE:
                        uart_debug_UartPutString("set volume: ");
                        if(data < 101) 
                        {
                        	print_6dec(data);
                            idac_vol_SetValue(data*2);
                            music_number = 5;
                        }
                        else uart_debug_UartPutString("wrong value");
                        break;
                    case CYBLE_TEST_SERVICE_POWER_CHAR_HANDLE:
                        uart_debug_UartPutString("set pwm_1: ");
                        if(data < 101) 
                        {
                        	print_6dec(data);
							pwm_led_1_Stop();
                            pwm_led_1_WriteCompare(data*pwm_mult); 
                            music_number = 3;
                        }
                        else uart_debug_UartPutString("wrong value");
                        break;
                    case CYBLE_TEST_SERVICE_POWER_2_CHAR_HANDLE:
                        uart_debug_UartPutString("set pwm_2: ");
                        if(data < 101) 
                        {
                        	print_6dec(data);
							pwm_led_2_Stop();
                            pwm_led_2_WriteCompare(data*pwm_mult); 
                            music_number = 3;
                        }
                        else uart_debug_UartPutString("wrong value");
                        break;    
                    case CYBLE_TEST_SERVICE_CONTROL_CHAR_HANDLE:
                        uart_debug_UartPutString("control: ");
                        print_6dec(data);
                        switch(data)
                        {
                            case 2:
                            	uart_debug_UartPutString(" (work ch 1 start)");
                                music_number = 2;
                                pwm_led_1_Enable();
                                signal_led_on();
                                break;
                            case 3:
                            	uart_debug_UartPutString(" (work ch 1 end)");
                                pwm_led_1_Stop();
                                signal_led_off();
                                music_number = 3;
                                break;
                            case 4:
                            	uart_debug_UartPutString(" (bonding off)");
                            	cyBle_authInfo.bonding =  CYBLE_GAP_BONDING_NONE;
                            	music_number = 4;
                            	break;
                            case 5:
                            	uart_debug_UartPutString(" (bonding on)");
                                cyBle_authInfo.bonding =  CYBLE_GAP_BONDING;
                                music_number = 4;
                                break;
                            case 6:
                            	uart_debug_UartPutString(" (disconnect)");
                                CyBle_GapDisconnect(connectionHandle.bdHandle);
                                break;    
                            case 7:
                            	uart_debug_UartPutString(" (debug led on)");
                                debug_led_on();
                                break;  
                            case 8:
                            	uart_debug_UartPutString(" (debug led off)");
                                debug_led_off();
                                break;
                            case 9:
                            	uart_debug_UartPutString(" (work ch 2 start)");
                                music_number = 2;
                                pwm_led_2_Enable();
                                signal_led_on();
                                break;
                            case 10:
                            	uart_debug_UartPutString(" (work ch 2 end)");
                                pwm_led_2_Stop();
                                signal_led_off();
                                music_number = 3;
                                break;                                
                            default: break;
                        }
                        break;
                    case CYBLE_TEST_SERVICE_STATUS_CHAR_HANDLE:
                        uart_debug_UartPutString("set status: ");
                        print_6dec(data);
                        break;    
                    default:
                        uart_debug_UartPutString("unknown: ");
                        print_6dec(data);
                        break;
                }

                CyBle_GattsWriteRsp(connectionHandle);
            }
            else uart_debug_UartPutString ("\e[31mGATT ERROR\e[39m");
            break; 
        case CYBLE_EVT_GATT_DISCONNECT_IND:
            pwm_led_1_Stop();
            pwm_led_1_WriteCompare(0);
            pwm_led_2_Stop();
            pwm_led_2_WriteCompare(0);
            conn_led_off();
            music_number = 6;
            uart_debug_UartPutString("gatt disconnect");
            break;
        case CYBLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ:
            uart_debug_UartPutString("read request");
			uart_debug_UartPutString(string_info);
            switch(((CYBLE_GATTS_CHAR_VAL_READ_REQ_T*)eventParam)->attrHandle)
            {
                case CYBLE_TEST_SERVICE_VOLUME_CHAR_HANDLE: uart_debug_UartPutString("volume"); break;
                case CYBLE_TEST_SERVICE_POWER_CHAR_HANDLE: uart_debug_UartPutString("pwm_1"); break;
                case CYBLE_TEST_SERVICE_CONTROL_CHAR_HANDLE: uart_debug_UartPutString("control"); break;
                case CYBLE_TEST_SERVICE_STATUS_CHAR_HANDLE: uart_debug_UartPutString("status"); break;
                case CYBLE_TEST_SERVICE_POWER_2_CHAR_HANDLE: uart_debug_UartPutString("pwm_2"); break;
                case 1: uart_debug_UartPutString("Handle of the GAP service"); break;
                case 3: uart_debug_UartPutString("Handle of the Device Name characteristic"); break;
                case 5: uart_debug_UartPutString("Handle of the Appearance characteristic"); break;
                default: 
                    uart_debug_UartPutString("unknown: "); 
                    print_6dec((((CYBLE_GATTS_CHAR_VAL_READ_REQ_T*)eventParam)->attrHandle));
                    break;
            }
            break;
        case CYBLE_EVT_GATTC_HANDLE_VALUE_IND:
            uart_debug_UartPutString("Indication data received from server device");
            result = CyBle_GapDisconnect(connectionHandle.bdHandle);
            print_function_state("CyBle_GapDisconnect", &result);       
            break;
        default:
            uart_debug_UartPutString("unknown: ");
            print_2hex(event);
            break;
	}
}
CY_ISR(halfsec_irq_handler)
{
    timer_halfsec_ClearInterrupt(timer_halfsec_INTR_MASK_TC);
    if((music_number > 0)&&(music_work != music_number))
	{
		music_work = music_number;
		music_number = 0;
		music_counter = 0;
	}
    if(music_work > 0)
    {
        uint16_t temp = descriptors[music_work-1].pointer[music_counter];
        pwm_sound_WritePeriod(temp);
        pwm_sound_WriteCompare(temp>>1);
        music_counter++;
        if(music_counter == descriptors[music_work-1].lenght)
        {
            if(descriptors[music_work-1].repeat) music_number = music_work;
            music_work = 0;
            pwm_sound_WriteCompare(0);
        }
    }
}
CY_ISR(slave_irq_handler)
{
    slave_in_ClearInterrupt();
    uart_debug_UartPutString(string_irq);
    uart_debug_UartPutString("slave state change");
    slave_chk();
}
void board_init(void)
{
#if TEST
    led_green_Write(1);
    led_red_Write(1);
#else
#endif
    conn_led_off();
}
void conn_led_on(void)
{
#if TEST
    led_blue_Write(0);
#else
    led_control_Write(1);
#endif       
}
void conn_led_off(void)
{
#if TEST
    led_blue_Write(1);
#else
    led_control_Write(0);
#endif       
}
void signal_led_on(void)
{
#if TEST
    led_green_Write(0);
#else
    led_signal_Write(1);
#endif       
}
void signal_led_off(void)
{
#if TEST
    led_green_Write(1);
#else
    led_signal_Write(0);
#endif       
}
void main(void)
{
    CyGlobalIntEnable;
    //pwm_freq = 12m/(128*1000)=93.75Hz
    pwm_led_1_Start();
    pwm_led_1_WriteCompare(0);
    pwm_led_2_Start();
    pwm_led_2_WriteCompare(0);
    uart_debug_Start();
    idac_vol_Start();
    idac_vol_SetValue(200);
    pwm_sound_Start();
    pwm_sound_WriteCompare(0);
    irq_0_StartEx(halfsec_irq_handler);
    irq_1_StartEx(slave_irq_handler);
    timer_halfsec_Start();
    music_number = 1;
    status = 0xaa;
    pwm_mult = pwm_led_1_ReadPeriod()/100;
    uart_debug_UartPutString("\r\nLEDForester");
    //uart_debug_UartPutChar(0xae);
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("software version: 201127");
    uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("software building date: ");
    uart_debug_UartPutString(__DATE__);
    result = CyBle_Start(ble_event_handler);
    print_function_state("CyBle_Start", &result);
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("BLE stack version: ");
    if(CyBle_GetStackLibraryVersion(&ble_ver) == CYBLE_ERROR_OK)
    {
        print_2hex(ble_ver.majorVersion);
        uart_debug_UartPutChar(0x2e);
        print_2hex(ble_ver.minorVersion);
        uart_debug_UartPutChar(0x2e);
        print_2hex(ble_ver.patch);
    }
    else uart_debug_UartPutString("\e[31mERROR\e[39m");
	uart_debug_UartPutString(string_info);
	uart_debug_UartPutString("PIN: ");
    uint8_t flash_x = CY_GET_REG8(CYREG_SFLASH_DIE_X);
    uint8_t flash_y = CY_GET_REG8(CYREG_SFLASH_DIE_Y);
    uint8_t flash_wafer = CY_GET_REG8(CYREG_SFLASH_DIE_WAFER);
    passkey = ((flash_wafer<<16)|(flash_x<<8)|flash_y)%1000000;
    print_6dec(passkey);
    temp = flash_x & 0xf;
    if(temp>9) dev_name[15] = temp + 0x37;
    else dev_name[15] = temp + 0x30;
    temp = ((flash_x & 0x30)>>4)|((flash_y & 0x3) << 2);
    if(temp>9) dev_name[14] = temp + 0x37;
    else dev_name[14] = temp + 0x30;
    temp = (flash_y & 0x3c) >> 2;
    if(temp>9) dev_name[13] = temp + 0x37;
    else dev_name[13] = temp + 0x30;
    temp = flash_wafer & 0xf;
    if(temp>9) dev_name[12] = temp + 0x37;
    else dev_name[12] = temp + 0x30;
    result = CyBle_GapSetLocalName(dev_name);
    print_function_state("CyBle_GapSetLocalName", &result);
    result = CyBle_GapGetLocalName(dev_name);
    print_function_state("CyBle_GapGetLocalName", &result);
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("device name: ");
    uart_debug_UartPutString(dev_name);
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("bonding: ");
    if(cyBle_authInfo.bonding == CYBLE_GAP_BONDING_NONE) uart_debug_UartPutString("OFF");
    else 
    {
        uart_debug_UartPutString("ON");
        result = CyBle_GapGetBondedDevicesList(&bondedDeviceList);
        print_function_state("CyBle_GapGetBondedDevicesList", &result);
        if(result == CYBLE_ERROR_OK)
        {
			uart_debug_UartPutString(string_info);
            uart_debug_UartPutString("Bonded device list");
            if(bondedDeviceList.count != 0)
            {
                for(uint8_t i=0;i<bondedDeviceList.count;i++)
                {
                    uart_debug_UartPutString("\r\n[");
                    uart_debug_UartPutChar(i+0x31);
                    uart_debug_UartPutString("] address type: ");
                    if(bondedDeviceList.bdAddrList[i].type == 0) uart_debug_UartPutString("public");
                    else uart_debug_UartPutString("random");
                    uart_debug_UartPutString("\r\n[");
                    uart_debug_UartPutChar(i+0x31);
                    uart_debug_UartPutString("] address: ");
                    print_address(bondedDeviceList.bdAddrList[i].bdAddr);
                    result = CyBle_GapGetPeerBdHandle(&temp, &bondedDeviceList.bdAddrList[i]);
                    print_function_state("CyBle_GapGetPeerBdHandle", &result);
                    bondedDeviceKeysFlag = 7;
                    result = CyBle_GapGetPeerDevSecurityKeyInfo(temp, &bondedDeviceKeysFlag, &securityKeys);
                    print_function_state("CyBle_GapGetPeerDevSecurityKeyInfo", &result);
                    key_info_print(&securityKeys);
                }
            }
            else uart_debug_UartPutString("\r\n[0] No bonded device");
        }
    }
    slave_chk();
    board_init();
    music_number = 6;
    while(1)
    {
        CyBle_ProcessEvents();
#if (CYBLE_BONDING_REQUIREMENT == CYBLE_BONDING_YES)        
        if(cyBle_pendingFlashWrite)
        {
            result = CyBle_StoreBondingData(0);
            print_function_state("CyBle_StoreBondingData", &result);
        }
#endif        
    }
}
void print_auth_info(CYBLE_GAP_AUTH_INFO_T* pointer)
{
	uart_debug_UartPutString(string_info);
	uart_debug_UartPutString("security: ");
	switch(pointer->security)
	{
		case 0: uart_debug_UartPutString("NO"); break;
	    case 1: uart_debug_UartPutString("unauthenticated pairing with encryption"); break;
	    case 2: uart_debug_UartPutString("authenticated pairing with encryption"); break;
	    case 3: uart_debug_UartPutString("secured connection"); break;
	}
	uart_debug_UartPutString(string_info);
	uart_debug_UartPutString("bonding: ");
	if(pointer->bonding == CYBLE_GAP_BONDING_NONE) uart_debug_UartPutString("NO");
	else uart_debug_UartPutString("YES");
}
void print_conn_param(CYBLE_GAP_CONN_PARAM_UPDATED_IN_CONTROLLER_T* pointer)
{
	uart_debug_UartPutString(string_info);
	uart_debug_UartPutString("status: ");
	print_hci_error(pointer->status);
	uart_debug_UartPutString(string_info);
	uart_debug_UartPutString("connection interval: ");
	print_6dec(pointer->connIntv);
	uart_debug_UartPutString(string_info);
	uart_debug_UartPutString("slave latency for the connection: ");
	print_6dec(pointer->connLatency);
	uart_debug_UartPutString(string_info);
	uart_debug_UartPutString("supervision timeout for the LE link: ");
	print_6dec(pointer->supervisionTO);
}
void print_6dec(uint32_t data)
{
	char chars[7];
    chars[0] = data/100000 + 0x30;
    chars[1] = ((data/10000)%10) + 0x30;
    chars[2] = ((data/1000)%10) + 0x30;
    chars[3] = ((data/100)%10) + 0x30;
    chars[4] = ((data/10)%10) + 0x30;
    chars[5] = (data%10) + 0x30;
    chars[6] = 0;
    uart_debug_UartPutString(chars);
}
void print_hci_error(CYBLE_HCI_ERROR_T data)
{
    switch(data)
    {
        case CYBLE_HCI_NO_CONNECTION_ERROR:
            uart_debug_UartPutString("unknown connection identifier");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_USER_ERROR:
            uart_debug_UartPutString("remote user terminated connection");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_LOCAL_HOST_ERROR:
            uart_debug_UartPutString("connection terminated by local host");
            break;
        case CYBLE_HCI_COMMAND_SUCCEEDED:
            uart_debug_UartPutString("command success");
            break;
        case CYBLE_HCI_CONNECTION_FAILED_TO_BE_ESTABLISHED:
            uart_debug_UartPutString("connection failed to be established");
            break;
        case CYBLE_HCI_CONNECTION_TIMEOUT_ERROR:
            uart_debug_UartPutString("connection timeout");
            break;
        case CYBLE_HCI_LL_RESPONSE_TEMEOUT_ERROR:
            uart_debug_UartPutString("LL Response Timeout");
            break;
        case CYBLE_HCI_CONNECTION_TERMINATED_DUE_TO_MIC_FAILURE:
            uart_debug_UartPutString("Connection Terminated due to MIC Failure");
            break;            
        default:
        	print_6dec(data);
            break;
    }
}
void print_2hex(uint8_t data)
{
    uint8_t temp[2];
	temp[0] = (data&0xf0)>>4;
	temp[1] = data&0x0f;
    for(uint8_t i=0; i<2; i++)
    {
        if(temp[i] < 10) uart_debug_UartPutChar(temp[i] + 0x30);
        else uart_debug_UartPutChar(temp[i] + 0x57);
        
    }
}
void slave_chk(void)
{
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("slave state: ");
    if(slave_in_Read() == 1) uart_debug_UartPutString("connect");
    else uart_debug_UartPutString("disconnect");
}
void print_function_state(char* name, CYBLE_API_RESULT_T* result)
{
    if(*result == CYBLE_ERROR_OK) 
    {
        uart_debug_UartPutString(string_ok);
        uart_debug_UartPutString(name);
    }
    else
    {
        uart_debug_UartPutString(string_fail);
        uart_debug_UartPutString(name);
		uart_debug_UartPutString(string_info);
        switch(*result)
        {
            case CYBLE_ERROR_INVALID_PARAMETER:
                uart_debug_UartPutString("At least one of the input parameters is invalid");
                break;
            case CYBLE_ERROR_INVALID_OPERATION:
                uart_debug_UartPutString("Operation is not permitted");
                break;
            case CYBLE_ERROR_MEMORY_ALLOCATION_FAILED:
                uart_debug_UartPutString("An internal error occurred in the stack");
                break;                
            case CYBLE_ERROR_INSUFFICIENT_RESOURCES:
                uart_debug_UartPutString("Insufficient resources to perform requested operation");
                break;
            case CYBLE_ERROR_OOB_NOT_AVAILABLE:
                uart_debug_UartPutString("OOB data not available");
                break;
            case CYBLE_ERROR_NO_CONNECTION:
                uart_debug_UartPutString("Connection is required to perform requested operation. Connection not present");
                break;         
            case CYBLE_ERROR_NO_DEVICE_ENTITY:
                uart_debug_UartPutString("No device entity to perform requested operation");
                break;
            case CYBLE_ERROR_REPEATED_ATTEMPTS:
                uart_debug_UartPutString("Attempted repeat operation is not allowed");
                break;
            case CYBLE_ERROR_GAP_ROLE:
                uart_debug_UartPutString("GAP role is incorrect");
                break;                
            case CYBLE_ERROR_TX_POWER_READ:
                uart_debug_UartPutString("Error reading TC power");
                break;
            case CYBLE_ERROR_BT_ON_NOT_COMPLETED:
                uart_debug_UartPutString("BLE Initialization failed");
                break;
            case CYBLE_ERROR_SEC_FAILED:
                uart_debug_UartPutString("Security operation failed");
                break;
            case CYBLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE:
                uart_debug_UartPutString("Invalid attribute handle");
                break;
            case CYBLE_ERROR_DEVICE_ALREADY_EXISTS:
                uart_debug_UartPutString("Device cannot be added to whitelist as it has already been added");
                break;
            case CYBLE_ERROR_FLASH_WRITE_NOT_PERMITED:
                uart_debug_UartPutString("Write to flash is not permitted");
                break;                
            case CYBLE_ERROR_HARDWARE_FAILURE:
                uart_debug_UartPutString("Hardware Failure");
                break;
            case CYBLE_ERROR_UNSUPPORTED_FEATURE_OR_PARAMETER_VALUE:
                uart_debug_UartPutString("Unsupported feature or parameter value");
                break;
            case CYBLE_ERROR_FLASH_WRITE:
                uart_debug_UartPutString("Error in flash Write");
                break;         
            case CYBLE_ERROR_CONTROLLER_BUSY:
                uart_debug_UartPutString("Controller Busy");
                break;
            case CYBLE_ERROR_MAX:
                uart_debug_UartPutString("All other errors not covered in the above list map to this error code");
                break;                
            default:
                print_2hex(*result);
                break;
        }
    }
}
void print_address(uint8_t* addr)
{
    print_2hex(addr[5]);
    uart_debug_UartPutChar(0x3a);
    print_2hex(addr[4]);
    uart_debug_UartPutChar(0x3a);
    print_2hex(addr[3]);
    uart_debug_UartPutChar(0x3a);
    print_2hex(addr[2]);
    uart_debug_UartPutChar(0x3a);
    print_2hex(addr[1]);
    uart_debug_UartPutChar(0x3a);
    print_2hex(addr[0]);
}
void key_info_print(CYBLE_GAP_SMP_KEY_DIST_T* ptr)
{
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("long term key: ");
    for(uint8_t i=0; i<CYBLE_GAP_SMP_LTK_SIZE; i++) print_2hex(ptr->ltkInfo[i]);
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("encrypted diversifier and random number: ");
    for(uint8_t i=0; i<CYBLE_GAP_SMP_MID_INFO_SIZE; i++) print_2hex(ptr->midInfo[i]);
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("identity resolving key: ");
    for(uint8_t i=0; i<CYBLE_GAP_SMP_IRK_SIZE; i++) print_2hex(ptr->irkInfo[i]);
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("address type: ");
    if(ptr->idAddrInfo[0] == 0) uart_debug_UartPutString("public device");
    else uart_debug_UartPutString("static random");
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("address: ");
    print_address(&(ptr->idAddrInfo[1]));
	uart_debug_UartPutString(string_info);
    uart_debug_UartPutString("connection signature resolving key: ");
    for(uint8_t i=0; i<CYBLE_GAP_SMP_CSRK_SIZE; i++) print_2hex(ptr->csrkInfo[i]);
}
void debug_led_on(void)
{
#if TEST
    led_red_Write(0);
#else
    led_debug_Write(1);
#endif       
}
void debug_led_off(void)
{
#if TEST
    led_red_Write(1);
#else
    led_debug_Write(0);
#endif       
}
