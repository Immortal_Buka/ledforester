package com.example.dav.ledforester41;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.dav.ledforester41.R.string.notifications;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_DATA =
            "com.example.dav.ledforester3.EXTRA_DATA";
    public final static String ACTION_GATT_CONNECTED =
            "com.example.dav.ledforester3.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_CONNECTING =
            "com.example.dav.ledforester3.ACTION_GATT_CONNECTING";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.dav.ledforester3.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_DISCONNECTED_CAROUSEL =
            "com.example.dav.ledforester3.ACTION_GATT_DISCONNECTED_CAROUSEL";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.dav.ledforester3.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_CHARACTERISTIC_CHANGED =
            "com.example.dav.ledforester3.ACTION_CHARACTERISTIC_CHANGED";

    public final static String ACTION_DATA_AVAILABLE =
            "com.example.dav.ledforester3.ACTION_DATA_AVAILABLE";
    public final static String ACTION_OTA_DATA_AVAILABLE =
            "com.example.dav.ledforester3.ACTION_OTA_DATA_AVAILABLE";
    public final static String ACTION_GATT_DISCONNECTED_OTA =
            "com.example.dav.ledforester3.ACTION_GATT_DISCONNECTED_OTA";
    public final static String ACTION_GATT_CONNECT_OTA =
            "com.example.dav.ledforester3.ACTION_GATT_CONNECT_OTA";
    public final static String ACTION_GATT_SERVICES_DISCOVERED_OTA =
            "com.example.dav.ledforester3.ACTION_GATT_SERVICES_DISCOVERED_OTA";
    public final static String ACTION_GATT_CHARACTERISTIC_ERROR =
            "com.example.dav.ledforester3.ACTION_GATT_CHARACTERISTIC_ERROR";
    public final static String ACTION_GATT_SERVICE_DISCOVERY_UNSUCCESSFUL =
            "com.example.dav.ledforester3.ACTION_GATT_SERVICE_DISCOVERY_UNSUCCESSFUL";
    public final static String ACTION_PAIR_REQUEST =
            "android.bluetooth.device.action.PAIRING_REQUEST";
    public final static String ACTION_WRITE_COMPLETED =
            "android.bluetooth.device.action.ACTION_WRITE_COMPLETED";
    public final static String ACTION_WRITE_FAILED =
            "android.bluetooth.device.action.ACTION_WRITE_FAILED";
    public final static String ACTION_WRITE_SUCCESS =
            "android.bluetooth.device.action.ACTION_WRITE_SUCCESS";

    public final static String ACTION_PIN_PASSED = "com.example.dav.ledforester3.ACTION_PIN_PASSED";
    public final static String ACTION_PIN_CANCELED = "com.example.dav.ledforester3.ACTION_PIN_CANCELED";
    public final static String ACTION_PIN_REQUEST = "com.example.dav.ledforester3.ACTION_PIN_REQUEST";
    public final static String ACTION_PIN_NOT_NEEDED = "com.example.dav.ledforester3.ACTION_PIN_NOT_NEEDED";

    public final static String ACTION_BOND_NONE = "com.example.dav.ledforester3.ACTION_BOND_NONE";
    public final static String ACTION_BOND_BONDED = "com.example.dav.ledforester3.ACTION_BOND_BONDED";
    public final static String ACTION_BOND_BONDING = "com.example.dav.ledforester3.ACTION_BOND_BONDING";

    final static String TAG_PREF = "FRAGMENT_PREF";
    final static String TAG_MAIN = "FRAGMENT_MAIN";
    final static String TAG_HELP = "FRAGMENT_HELP";
    final static String TAG_CONNECT = "FRAGMENT_CONNECT";
    final static String TAG_DATA = "DATA_BUNDLE";
    final static String DEVICE_NAME = "DEVICE_NAME";
    private String tag;
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public static final String EXTRAS_DEVICE_BOND_STATE = "BOND_STATE";
    public static final int REQUEST_BT_SETTINGS = 3;

    BroadcastReceiver statusBarReceiver;
    private final static String TAG = MainActivity.class.getSimpleName();
    private TextView mConnectionState;
    private String mConnectionStateText;
    private TextView mDataField;
    private String mDeviceName;

    private int mFragNumber = 1;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private boolean mBonded = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private FragmentManager fragmentManager;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private int state;

    String languageChangedAction = "LANGUAGE_CHANGED_ACTION";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragmentManager = getFragmentManager();
        tag = TAG_MAIN;
        Fragment fragment = MainFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.container, fragment, tag).commit();
        Intent data = getIntent();
        mDeviceAddress = data.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mDeviceName = data.getStringExtra(EXTRAS_DEVICE_NAME);
        state=data.getIntExtra(EXTRAS_DEVICE_BOND_STATE, BluetoothDevice.BOND_NONE);

        switch(state){
            case BluetoothDevice.BOND_BONDING:

                break;

            case BluetoothDevice.BOND_BONDED:
                broadcastUpdate(ACTION_PIN_NOT_NEEDED);
                break;

            case BluetoothDevice.BOND_NONE:
               mBonded= false;

                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            return true;
        }
        else if (id == R.id.action_quit){
            closeGatt();
            try{
                unbindService(mServiceConnection);
            } catch (IllegalArgumentException e){
            }
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
            if (mBluetoothLeService.connect(mDeviceAddress)) {
                BluetoothDevice device = mBluetoothLeService.getDevice(mDeviceAddress);
                //    startBluetoothPairing(device);
                // Toast.makeText(getBaseContext(), "connected with" + device.getName(), Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(getBaseContext(), "not connected", Toast.LENGTH_SHORT).show();
            }
            //    mConnectionStateText = "Connected";
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
            //  Toast.makeText(getBaseContext(), "Service disconnected", Toast.LENGTH_SHORT).show();
            //  mConnectionStateText = "Disconnected";
        }
    };

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        registerReceiver(mPairReceiver, bondFilter);
        if (mBluetoothLeService == null) {
            Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
            bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

            if (mDeviceAddress == null) {
                // showToast("Device adress is null");
                return;
            }
        }
    }

    IntentFilter langChangedFilter = new IntentFilter(languageChangedAction);

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBluetoothLeService.close();
        unregisterReceiver(mGattUpdateReceiver);
        unregisterReceiver(mPairReceiver);
    }



    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_msg_main)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                closeGatt();
                if (mConnected) {
                    try {
                        unbindService(mServiceConnection);
                    } catch (IllegalArgumentException e) {
                    }
                }
                finish();
            }
        });

     AlertDialog alert = builder.create();
        alert.show();
    }

    public void sendData(byte[] value, String uuid) {
        mBluetoothLeService.writeCharacteristic(value, uuid);
    }

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                //displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            } else if (BluetoothLeService.ACTION_GATT_SERVICE_DISCOVERY_UNSUCCESSFUL.equals(action)) {
                //showToast("Insufficient authentification");
            }
        }
    };


    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICE_DISCOVERY_UNSUCCESSFUL);
        return intentFilter;
    }

    public void closeGatt() {
        mBluetoothLeService.disconnect();
        mBluetoothLeService.close();
    }

    public void reconnect(){
        closeGatt();
        mBluetoothLeService.connect(mDeviceAddress);
    }

    IntentFilter bondFilter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);

    private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    broadcastUpdate(ACTION_PIN_PASSED);
                    return;

                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {
                    broadcastUpdate(ACTION_PIN_CANCELED);
                    return;
                } else if (state == BluetoothDevice.BOND_BONDING) {
                    broadcastUpdate(ACTION_PIN_REQUEST);
                    return;
                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDING) {
                    broadcastUpdate(ACTION_PIN_CANCELED);
                    return;
                } else if (state == BluetoothDevice.BOND_BONDED) {
                    broadcastUpdate(ACTION_PIN_NOT_NEEDED);
                    return;
                }
            }
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    public boolean charIsPresent(String charUUID){
        return mBluetoothLeService.checkCharacteristic(charUUID);
    }

}
