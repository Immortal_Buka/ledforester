package com.example.dav.ledforester41;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.appwidget.AppWidgetManager;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;

import static java.lang.Thread.sleep;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment {

    public static Fragment newInstance() {
        MainFragment mainFragment = new MainFragment();
        return mainFragment;
    }

    //variables and constants
    private int powerInt;
    private int timeInt;
    private int volume;
    private long total;
    private CountDownTimer cdt = null;
    private int sec;
    private int min;
    private int mConnectionState;
    private String time = null;
    private String timerMin, timerSec;
    TextView tv;
    private final String volumeUUID = "00000000-0000-1000-8000-00805f9b34fb";
    private final String powerUUID = "14d220e1-b226-4222-9d4b-ea5162c3f5f5";
    private final String controlUUID = "f8f18123-725f-41cd-8b11-1b6d47bfa56a";
    private final String checkUUID = "25c3fcaa-e126-44c7-a305-46a8dc80e488";
    private static final int TURN_ON = 2;
    private static final int TURN_OFF = 3;
    private boolean isTicking = false;
    public boolean mConnected;
    public boolean mBonded;
    private boolean pinPassed;
    private AlertDialog alert;
    final int[] i = {1};
    // private i[0];

    //UIElements
    NumberPicker powerPicker;
    NumberPicker timePicker;
    ImageButton buttonStart;
    ImageButton button;
    SeekBar seekBar;


    BroadcastReceiver statusBarReceiver;
    IntentFilter statusBarIntentFilter;

    public final static String EXTRA_DATA =
            "com.example.dav.ledforester3.EXTRA_DATA";
    public final static String ACTION_GATT_CONNECTED =
            "com.example.dav.ledforester3.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_CONNECTING =
            "com.example.dav.ledforester3.ACTION_GATT_CONNECTING";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.dav.ledforester3.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_DISCONNECTED_CAROUSEL =
            "com.example.dav.ledforester3.ACTION_GATT_DISCONNECTED_CAROUSEL";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.dav.ledforester3.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_CHARACTERISTIC_CHANGED =
            "com.example.dav.ledforester3.ACTION_CHARACTERISTIC_CHANGED";

    public final static String ACTION_PIN_PASSED = "com.example.dav.ledforester3.ACTION_PIN_PASSED";
    public final static String ACTION_PIN_CANCELED = "com.example.dav.ledforester3.ACTION_PIN_CANCELED";
    public final static String ACTION_PIN_REQUEST = "com.example.dav.ledforester3.ACTION_PIN_REQUEST";
    public final static String ACTION_PIN_NOT_NEEDED = "com.example.dav.ledforester3.ACTION_PIN_NOT_NEEDED";

    public final static String ACTION_BOND_NONE = "com.example.dav.ledforester3.ACTION_BOND_NONE";
    public final static String ACTION_BOND_BONDED = "com.example.dav.ledforester3.ACTION_BOND_BONDED";
    public final static String ACTION_BOND_BONDING = "com.example.dav.ledforester3.ACTION_BOND_BONDING";


    public final static String ACTION_DATA_AVAILABLE =
            "com.example.dav.ledforester3.ACTION_DATA_AVAILABLE";
    public final static String ACTION_OTA_DATA_AVAILABLE =
            "com.example.dav.ledforester3.ACTION_OTA_DATA_AVAILABLE";
    public final static String ACTION_GATT_DISCONNECTED_OTA =
            "com.example.dav.ledforester3.ACTION_GATT_DISCONNECTED_OTA";
    public final static String ACTION_GATT_CONNECT_OTA =
            "com.example.dav.ledforester3.ACTION_GATT_CONNECT_OTA";
    public final static String ACTION_GATT_SERVICES_DISCOVERED_OTA =
            "com.example.dav.ledforester3.ACTION_GATT_SERVICES_DISCOVERED_OTA";
    public final static String ACTION_GATT_CHARACTERISTIC_ERROR =
            "com.example.dav.ledforester3.ACTION_GATT_CHARACTERISTIC_ERROR";
    public final static String ACTION_GATT_SERVICE_DISCOVERY_UNSUCCESSFUL =
            "com.example.dav.ledforester3.ACTION_GATT_SERVICE_DISCOVERY_UNSUCCESSFUL";
    public final static String ACTION_PAIR_REQUEST =
            "android.bluetooth.device.action.PAIRING_REQUEST";
    public final static String ACTION_WRITE_COMPLETED =
            "android.bluetooth.device.action.ACTION_WRITE_COMPLETED";
    public final static String ACTION_WRITE_FAILED =
            "android.bluetooth.device.action.ACTION_WRITE_FAILED";
    public final static String ACTION_WRITE_SUCCESS =
            "android.bluetooth.device.action.ACTION_WRITE_SUCCESS";


    public static boolean setNumberPickerTextSize(NumberPicker anyNumberPicker, float size) {
        final int count = anyNumberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = anyNumberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = anyNumberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(anyNumberPicker)).setTextSize(size);
                    ((EditText) child).setTextSize(size-70);
                    anyNumberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                    Log.w("PickerTextColor", e);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }
        return false;
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color) {
        final int count = numberPicker.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = numberPicker.getChildAt(i);
            if (child instanceof EditText) {
                try {
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText) child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                } catch (NoSuchFieldException e) {
                    Log.w("setPickerTextColor", e);
                } catch (IllegalAccessException e) {
                    Log.w("setPickerTextColor", e);
                } catch (IllegalArgumentException e) {
                    Log.w("setPickerTextColor", e);
                }
            }
        }
        return false;
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(ContextCompat.getColor(getActivity(), color));
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_main, container, false);
        final Context context = getActivity();
        button = (ImageButton) v.findViewById(R.id.button);
        //initialize NP
        powerPicker = (NumberPicker) v.findViewById(R.id.powerPicker);
        timePicker = (NumberPicker) v.findViewById(R.id.timePicker);
        powerPicker.setMaxValue(100);
        powerPicker.setMinValue(0);
        powerPicker.setWrapSelectorWheel(false);
        final String[] timeArray = getResources().getStringArray(R.array.time_array);
        timePicker.setMaxValue(timeArray.length - 1);
        timePicker.setMinValue(0);
        timePicker.setWrapSelectorWheel(false);
        timePicker.setDisplayedValues(timeArray);
        int childCount = timePicker.getChildCount();
        WindowManager wm = (WindowManager)    context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        int height;
        try {
            display.getRealSize(size);
            height = size.y;
        } catch (NoSuchMethodError e) {
            height = display.getHeight();
        }
        int npSize = height/17;
        setNumberPickerTextSize(timePicker, npSize);
        setNumberPickerTextSize(powerPicker, npSize);


        //Using SharPref to start NPs on prev position

        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        int prevPower = sharedPref.getInt(getString(R.string.sp_power_key), powerInt);
        int prevTime = sharedPref.getInt(getString(R.string.sp_time_key), timeInt);
        int prevVolume = sharedPref.getInt(getString(R.string.sp_volume_key), volume);
        powerPicker.setValue(prevPower);
        timePicker.setValue(prevTime);

        for (int i = 0; i < childCount; i++) {
            View childView = timePicker.getChildAt(i);
            if (childView instanceof EditText) {
                EditText et = (EditText) childView;
                et.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
        }



        //initializing other views of MainFragment
        tv = (TextView) v.findViewById(R.id.statusBar);
        tv.setText(getString(R.string.wait_for_update));
        final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progressBar2);
        buttonStart = (ImageButton) v.findViewById(R.id.buttonStart);


        seekBar = (SeekBar)v.findViewById(R.id.seekBar3);
        seekBar.setMax(100);
        seekBar.setProgress(prevVolume);
        volume = seekBar.getProgress();

        //AlertDialog for buttons and SeekBar
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_title))
                .setMessage(getString(R.string.dialog_msg))
                .setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alert = builder.create();

        class MyTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                sendData(powerInt, powerUUID);
                //((MainActivity)getActivity()).sendData(hexStringToByteArray(Integer.toHexString(powerInt)),powerUUID);
                //((MainActivity)getActivity()).sendData(hexStringToByteArray(Integer.toHexString(volume)),volumeUUID);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                progressBar.setVisibility(View.GONE);
                buttonStart.setVisibility(View.VISIBLE);
                super.onPostExecute(result);
            }
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mConnected) alert.show();
                else {
                    powerInt = powerPicker.getValue();
                    timeInt = timePicker.getValue();
                    volume = seekBar.getProgress();
                    // this code counts time picked on TimePicker
                    if (timeInt > 5) {
                        timeInt = 5 * (timeInt - 4);
                    }
                    total = timeInt * 60000;
                    numberPickersOff();
                    button.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    MyTask myTask = new MyTask();
                    myTask.execute();
              }


             //   if (((MainActivity)getActivity()).charIsPresent(controlUUID)) showToast("Char is present");
           }
        });

        timePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                editor.putInt(getString(R.string.sp_time_key),newVal).apply();
                button.setVisibility(View.VISIBLE);
            }
        });
        powerPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                button.setVisibility(View.VISIBLE);
                editor.putInt(getString(R.string.sp_power_key), newVal).apply();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int lastVol;
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                lastVol=seekBar.getProgress();


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                volume = seekBar.getProgress();
                editor.putInt(getString(R.string.sp_volume_key), volume).apply();
                if (!mConnected){alert.show(); seekBar.setProgress(lastVol);}
                else  sendData(volume, volumeUUID);
            }
        });



        //setting case switch variable
        i[0] = 2;
        //setting Start Button work with CDT algorithm
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mConnected)
                    alert.show();
                else {

                    switch (i[0]) {
                        case 2: //start light
                            //((MainActivity) getActivity()).sendData(hexStringToByteArray(Integer.toHexString(TURN_ON)), controlUUID);
                            sendData(TURN_ON, controlUUID);

                            //Create new CDT and start countdown
                            cdt = new CountDownTimer(total, 1000) {
                                @Override
                                public void onTick(long l) {
                                    total = l;
                                    min = (int) (l / 60000);
                                    sec = (int) (l / 1000);
                                    sec = sec - 60 * min;
                                    if (min >= 10) {
                                        timerMin = ("" + min);
                                    } else {
                                        timerMin = ("0" + min);
                                    }
                                    if (sec >= 10) {
                                        timerSec = ("" + sec);
                                    } else {
                                        timerSec = ("0" + sec);
                                    }
                                    time = ("" + timerMin + ":" + timerSec);
                                    tv.setText(time);
                                    isTicking = true;
                                }

                                @Override
                                public void onFinish() {
                                    stopProcedure();
                                    tv.setText(R.string.sb_done);
                                    buttonStart.setImageResource(R.mipmap.ic_send);
                                    buttonStart.setBackgroundResource(R.drawable.rounded);
                                    numberPickersOn();
                                    isTicking = false;

                                }
                            }.start();
                            i[0] = 3;
                            buttonStart.setImageResource(R.mipmap.ic_stop);
                            buttonStart.setBackgroundResource(R.drawable.rounded_pink);
                            break;

                        case 3:
                            cdt.cancel();
                            isTicking = false;
                            stopProcedure();
                            buttonStart.setImageResource(R.mipmap.ic_send);
                            buttonStart.setBackgroundResource(R.drawable.rounded);
                            numberPickersOn();
                            i[0] = 2;
                        case 4:
                            cdt.cancel();
                            buttonStart.setImageResource(R.mipmap.ic_ready);
                            buttonStart.setBackgroundResource(R.drawable.rounded);
                            buttonStart.setVisibility(View.INVISIBLE);
                            numberPickersOn();
                            button.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }
        });

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setClickable(false);
                tv.setPaintFlags(0);
                ((MainActivity)getActivity()).reconnect();
                getActivity().registerReceiver(mGattUpdateReceiver,makeGattUpdateIntentFilter());
            }
        });
        tv.setClickable(false);
        numberPickersOff();
        return v;
    }



    @Override
    public void onResume(){
        getActivity().registerReceiver(mGattUpdateReceiver,makeGattUpdateIntentFilter());
        getActivity().registerReceiver(pinPassReceiver, makePinPassFilter());
        getActivity().registerReceiver(bondStateReceiver, bondStateFilter());
        super.onResume();
    }

    @Override
    public void onDestroy(){
        getActivity().unregisterReceiver(mGattUpdateReceiver);
        getActivity().unregisterReceiver(pinPassReceiver);
        getActivity().unregisterReceiver(bondStateReceiver);
        super.onDestroy();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    private void sendData(int mData, String UUID) {
      //  if (mConnected)
            ((MainActivity) getActivity()).sendData(hexStringToByteArray(Integer.toHexString(mData)), UUID);
      //  else alert.show();
    }

    public void stopProcedure(){
       // if (mConnected)
            ((MainActivity)getActivity()).sendData(hexStringToByteArray(Integer.toHexString(TURN_OFF)), controlUUID);
        //else Toast.makeText((MainActivity)getActivity(), "Connect first", Toast.LENGTH_SHORT).show();
    }


    private void numberPickersOff(){
        timePicker.setEnabled(false);
        powerPicker.setEnabled(false);
        setDividerColor(powerPicker, R.color.colorNPDividerOff);
        setDividerColor(timePicker,R.color.colorNPDividerOff);
        setNumberPickerTextColor(timePicker, ContextCompat.getColor(getActivity(),R.color.colorTextNPOff));
        setNumberPickerTextColor(powerPicker, ContextCompat.getColor(getActivity(),R.color.colorTextNPOff));
        // this disables seekbar
        seekBar.setEnabled(false);
    }


    private void numberPickersOn(){
        timePicker.setEnabled(true);
        powerPicker.setEnabled(true);
        setDividerColor(powerPicker, R.color.colorNPDividerOn);
        setDividerColor(timePicker,R.color.colorNPDividerOn);
        setNumberPickerTextColor(timePicker, ContextCompat.getColor(getActivity(),R.color.colorTextNPOn));
        setNumberPickerTextColor(powerPicker, ContextCompat.getColor(getActivity(),R.color.colorTextNPOn));
        seekBar.setEnabled(true);
    }


    private byte[] hexStringToByteArray(String s) {
        if (s.length()<2) s="0"+s;
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
                    .digit(s.charAt(i + 1), 16));
        }
        return data;
    }




    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                //mConnected=true;
              /*  if (!((MainActivity)getActivity()).charIsPresent(controlUUID)) {
                    numberPickersOff();
                    updateConnectionState(R.string.not_ledforester);
                    return;
                }*/
                   // numberPickersOn();
                   // updateConnectionState(R.string.connected);

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                if (isTicking) cdt.cancel();
                tv.setClickable(true);
                updateConnectionState(R.string.disconnected);
                resetButton();
                numberPickersOff();
              //  getActivity().registerReceiver(mGattUpdateReceiver,makeGattUpdateIntentFilter());
               // getActivity().registerReceiver(pinPassReceiver, makePinPassFilter());
                showToast(getString(R.string.toast_disconnected));
            }
        }
    };

    private void resetButton() {
        buttonStart.setImageResource(R.mipmap.ic_send);
        buttonStart.setBackgroundResource(R.drawable.rounded);
        numberPickersOn();
        button.setVisibility(View.VISIBLE);
        i[0] = 2;
    }

   private final BroadcastReceiver pinPassReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mBonded=false;
            String action = intent.getAction();
            if (action.equals(ACTION_PIN_PASSED)) {
                mConnected = true;
                numberPickersOn();
                updateConnectionState(R.string.connected);
                //showToast("PinPass: connected");
            }

            if (action.equals(ACTION_PIN_CANCELED)) {
                mBonded=false;
                ((MainActivity)getActivity()).closeGatt();
                updateConnectionState(R.string.pass_failed);
                numberPickersOff();
            }

            if (action.equals(ACTION_PIN_REQUEST)) {
                updateConnectionState(R.string.pin_requested);
            }

           /* if (action.equals(ACTION_PIN_NOT_NEEDED)){
                mConnected = true;
                numberPickersOn();
                updateConnectionState(R.string.connected);
                showToast("Pin not needed");
            }*/
        }
    };

    private final BroadcastReceiver bondStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals((ACTION_BOND_BONDED))){
                updateConnectionState(R.string.connected);
                numberPickersOn();
                mConnected=true;
               // showToast("bondState: connected");
            }
        }
    };

    private static IntentFilter bondStateFilter(){
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_BOND_BONDED);
        intentFilter.addAction(ACTION_BOND_BONDING);
        intentFilter.addAction(ACTION_BOND_BONDING);
        return intentFilter;
    }

    private static IntentFilter makePinPassFilter(){
     final IntentFilter intentFilter = new IntentFilter();
     intentFilter.addAction(ACTION_PIN_PASSED);
     intentFilter.addAction(ACTION_PIN_CANCELED);
     intentFilter.addAction(ACTION_PIN_REQUEST);
     intentFilter.addAction(ACTION_PIN_NOT_NEEDED);
     return intentFilter;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    private void updateConnectionState(final int resourceId) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (resourceId == R.string.disconnected || resourceId == R.string.pass_failed) {tv.setPaintFlags(tv.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
                tv.setClickable(true);}
                tv.setText(resourceId);
            }
        });
    }

    private void showToast(String text){
        Toast.makeText(getActivity(),text, Toast.LENGTH_SHORT).show();
    }


}
