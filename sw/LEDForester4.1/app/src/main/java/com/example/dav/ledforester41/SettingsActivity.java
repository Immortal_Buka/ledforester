package com.example.dav.ledforester41;


import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.support.v4.content.SharedPreferencesCompat;
import android.text.TextUtils;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

import java.util.List;
import java.util.Locale;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
    Locale mLocale;
    String languageChangedAction = "LANGUAGE_CHANGED_ACTION";

        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

        }
        @Override
        public void onResume(){
            super.onResume();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String language = prefs.getString(getString(R.string.language_switch), "");
            Intent intent = new Intent (languageChangedAction);
            if (language.contains("Русский")) {mLocale = new Locale("ru");
                sendBroadcast(intent);
                changeLang(getApplicationContext(), mLocale);
               // this.recreate();
            }
            if (language.contains("English")) {mLocale = new Locale("en");
                sendBroadcast(intent);
                changeLang(getApplicationContext(), mLocale);
              //  this.recreate();
            }
        }

    public static void changeLang(Context context, Locale locale) {
        Locale.setDefault(locale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }


}
